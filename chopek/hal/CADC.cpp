/*
 * ADC.cpp
 *
 *  Created on: 13-11-2013
 *      Author: Przemek Kieszkowski
 */

#include "CADC.h"
#include <compiler.h>
#include <stddef.h>

#include "hal.h"

#include "trace.h"

CADC adc;


#define CADC_SAMPLES_NUMBER 6

static uint16_t buffer[CADC_SAMPLES_NUMBER];

//static bool isr_fired = false;


static void adcISR(void)
{
//  isr_fired = true;
//  HAL_UartWriteByte('X');
  adc.fireFromISR();
}

void CADCdata::Init(ADCChannel_t chann, HAL_AdcVoltageReference_t vref)
{
  switch (chann)
  {
    case BATTERY:         channel = HAL_ADC_CHANNEL0; break;
    case STRATIFICATION:  channel = HAL_ADC_CHANNEL1; break;
    case STR_VCC:         channel = HAL_ADC_CHANNEL2; break;
    case EVCC:            channel = HAL_ADC_EVDD; break;
  }
  adcDescriptor.tty = ADC_0;
  adcDescriptor.resolution = RESOLUTION_10_BIT;
  adcDescriptor.sampleRate = ADC_4800SPS;
  adcDescriptor.voltageReference = vref;
  adcDescriptor.bufferPointer = buffer;
  adcDescriptor.selectionsAmount = CADC_SAMPLES_NUMBER;
  adcDescriptor.callback = NULL;
  unexpected = false;
  conv_data = 0;
  new_data = 0;
  new_data_count = 0;
}

bool CADCdata::startConversion()
{
  unexpected = false;
  if (eventMgr.registerEvent(this))
  {
    return adc.startConversion(this);
  }
  else
  {
    return false;
  }
}

void CADCdata::callback_fire(uint8_t fired_cnt)
{
  UNUSED(fired_cnt);

  adc.conversionFinished();
  eventMgr.unregisterEvent(this);
  conversionFinished();
}
// returns converted AC data
uint16_t  CADCdata::getConversionData()
{
  return conv_data;
}

void CADCdata::setData(uint16_t new_sample)
{
  // filtering out noise on low differences
  int difference = (int)((int)conv_data - (int)new_sample);
  if (difference < 0)
  {
    difference = -difference;
  }
  if ((difference > 0) &&
      (difference <= ADC_FLUCTUAION))
  {
//    TRACE_USER("diff = %d\n", difference);
    if (this->new_data == new_sample)
    {
      new_data_count++;
      if (new_data_count >= ADC_FLUCTUAION_COUNT)
      {
        // new value should be approved
        conv_data = new_sample;
        new_data  = new_sample;
        new_data_count = 0;
//        TRACE_USER("data = %d\n", conv_data);
      }
    }
    else
    {
      new_data = new_sample;
      new_data_count = 0;
//      TRACE_USER("new_data = %d\n", new_sample);
    }
  }
  else
  {
    conv_data = new_sample;
    new_data = new_sample;
    new_data_count = 0;
  }
}


void CADC::fireFromISR()
{
  if (adc.curr_conv_p != NULL)
  {
    adc.curr_conv_p->fireEvent();
  }
}

void CADC::Init()
{
  CRun::Init();
  adc_conversion_queue.Init();
  curr_conv_p = NULL;
}

bool CADC::startADC()
{
  curr_conv_p->adcDescriptor.callback = adcISR;
  if (0 != HAL_OpenAdc(&curr_conv_p->adcDescriptor))
  {
    TRACE_ERROR("HAL_OpenAdc error\n");
    curr_conv_p = NULL;
    return false;
  }
  if (0 != HAL_ReadAdc(&curr_conv_p->adcDescriptor, curr_conv_p->channel))
  {
    TRACE_ERROR("HAL_ReadAdc error\n");
    curr_conv_p = NULL;
    return false;
  }
  return true;
}

bool CADC::startConversion(CADCdata* adc_data_p)
{
  if (curr_conv_p == NULL)
  {
    curr_conv_p = adc_data_p;
    return startADC();
  }
  else
  {
    if (curr_conv_p != adc_data_p)
    {
      bool push_stat = adc_conversion_queue.Push(adc_data_p);
      // queue item
//      TRACE_USER("Conversion ongoing push_stat = %d\n", (int)push_stat);
//      TRACE_USER("Conversion ongoing main_fired = %d\n", (int)eventMgr.main_fired);
      return push_stat;
    }
    else
    {
      // can't queue the same item
      TRACE_ERROR("start return false\n");
      return false;
    }
  }
  TRACE_ERROR("start return false\n");
  return false;
}

void CADC::conversionFinished()
{
  HAL_CloseAdc(&curr_conv_p->adcDescriptor);
  curr_conv_p->setData(filterConversionData());
  curr_conv_p = NULL;
  // start next conversion - if queued

//  TRACE_USER("conversionFinished\n");

  curr_conv_p = (CADCdata*)adc_conversion_queue.Pop();
  if (curr_conv_p)
  {
    startADC();
  }
}

uint16_t CADC::filterConversionData()
{
  uint16_t  maxv  = 0;
  uint16_t  minv  = 0xFFFF;
  uint16_t  value = 0;
//#define CADC_SAMPLES_NUMBER 6
//static uint16_t buffer[CADC_SAMPLES_NUMBER];
  // find max/min
  for(int i = 0; i < CADC_SAMPLES_NUMBER; i++)
  {
    value += buffer[i];
    if (buffer[i] > maxv) maxv = buffer[i];
    if (buffer[i] < minv) minv = buffer[i];
  }
  // remove max and min value
  value -= maxv;
  value -= minv;
  // calculate average
  value /= (CADC_SAMPLES_NUMBER - 2);
  return value;
}

void CADC::run()
{
//  if (isr_fired)
//  {
//    if (curr_conv_p != NULL)
//    {
//      curr_conv_p->fireEvent();
//    }
//    isr_fired = false;
//  }
#if 0
  switch (state)
  {
    case IDLE:
      break;
    case CONVERTING:
    {
      break;
    }
    case CALLBACK:
    {
      if (adc_conversion_p != NULL)
      {
        adc_conversion_p->storeConv(conv_buff);
      }
      if (adc_conversion_p != NULL)
      {
        adc_conversion_p->converted();
      }
      HAL_CloseAdc(&adc_conversion_p->adcDescriptor);
      adc_conversion_p = NULL;
      state = IDLE;
      break;
    }
  }
#endif
}

bool CADC::isIdle()
{
  return curr_conv_p == NULL;
}

CADC::~CADC()
{
}
