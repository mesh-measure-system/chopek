/*
* halWDT.c
*
* Created: 2014-11-24 20:22:09
* Author: Przemek
*/

#include "halWDT.h"
#include <avr/wdt.h>

#if defined (__GNUC__)
volatile reset_cause_t	reset_cause __attribute__ ((section(".noinit")));
#else
#error Unsupported compiler.
#endif

// Function Pototype
void wdt_init(void) __attribute__((naked)) __attribute__((section(".init3")));


// Function Implementation
void wdt_init(void)
{
  // remember reset cause
  reset_cause = reset_cause_get_causes();

  MCUSR = 0;
  wdt_enable(WDTO_8S);
}

