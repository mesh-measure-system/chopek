/*
 * ADC.h
 *
 *  Created on: 13-11-2013
 *      Author: Przemek Kieszkowski
 */

#ifndef CADC_H_
#define CADC_H_

#include <stdint.h>
#include "CRun.h"
#include "CEventMgr.h"
#include "CFiFoQueue.h"
#include "adc.h"

enum ADCChannel_t {BATTERY, STRATIFICATION, STR_VCC, EVCC};

#define ADC_FLUCTUAION        2
#define ADC_FLUCTUAION_COUNT  7

class CADCdata : public CEvent, public CFiFoItem
{
  private:
    HAL_AdcDescriptor_t     adcDescriptor;
    HAL_AdcChannelNumber_t  channel;
    uint16_t                conv_data;
    bool                    unexpected;

    // for filtering low measurement differences
    uint16_t                new_data;
    uint8_t                 new_data_count;

    void setData(uint16_t new_sample);
  public:

    void Init(ADCChannel_t chann, HAL_AdcVoltageReference_t vref);
    bool startConversion();

    // function is called before start conversion on the selected channel
    // it's purpose is to setup peripheral (enable vcc and simillar)
    virtual void adcSetupChannel() = 0;

    // returns converted AC data
    uint16_t  getConversionData();

    // for CEvent
    virtual void callback_fire(uint8_t fired_cnt);

    // method should be overwrited for get information about conversion finish
    // please also call base method
    virtual void conversionFinished() = 0;

    friend class CADC;
};

class CADC : public CRun
{
  private:
    CFiFoQueue  adc_conversion_queue;
    CADCdata*   curr_conv_p;
    bool startConversion(CADCdata* adc_data_p);
    bool startADC();
    uint16_t filterConversionData();

  public:
    void Init();
    void conversionFinished();

    // methods for CRun
    virtual void run();
    virtual bool isIdle();

    static void fireFromISR();

    // for eclipse happiness
    virtual ~CADC();

    friend class CADCdata;
};

extern CADC adc;
#endif /* CADC_H_ */
