/**
 * \file halUart.c
 *
 * \brief ATmega256rfr2 UART implementation
 *
 * Copyright (C) 2012-2014, Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 * Modification and other use of this code is subject to Atmel's Limited
 * License Agreement (license.txt).
 *
 * $Id: halUart.c 9267 2014-03-18 21:46:19Z ataradov $
 *
 */

/*- Includes ---------------------------------------------------------------*/
#include <stdbool.h>
#include "hal.h"
#include "halUart.h"
#include "sysclk.h"
#include "config.h"

#include "trace.h"

/*- Definitions ------------------------------------------------------------*/

// #if HAL_UART_CHANNEL == 0
//   #define UBRRxH            UBRR0H
//   #define UBRRxL            UBRR0L
//   #define UCSRxA            UCSR0A
//   #define UCSRxB            UCSR0B
//   #define UCSRxC            UCSR0C
//   #define UDRx              UDR0
//   #define USARTx_UDRE_vect  USART0_UDRE_vect
//   #define USARTx_RX_vect    USART0_RX_vect
// #elif HAL_UART_CHANNEL == 1
//   #define UBRRxH            UBRR1H
//   #define UBRRxL            UBRR1L
//   #define UCSRxA            UCSR1A
//   #define UCSRxB            UCSR1B
//   #define UCSRxC            UCSR1C
//   #define UDRx              UDR1
//   #define USARTx_UDRE_vect  USART1_UDRE_vect
//   #define USARTx_RX_vect    USART1_RX_vect
// #else
//   #error Unsupported UART channel
// #endif

/*- Types ------------------------------------------------------------------*/
typedef struct
{
  uint16_t  head;
  uint16_t  tail;
  uint16_t  size;
  uint16_t  bytes;
  volatile uint8_t   *data;
} FifoBuffer_t;

/*- Variables --------------------------------------------------------------*/
// for uart 0
static volatile FifoBuffer_t  tx0Fifo;
static volatile uint8_t       tx0Data[HAL_UART_TX_FIFO_SIZE+1];

static volatile FifoBuffer_t  rx0Fifo;
static volatile uint8_t       rx0Data[HAL_UART_RX_FIFO_SIZE+1];

static volatile bool          tx0Running;
static volatile bool          udr0Empty;
static volatile bool          new0Data;
unsigned char                 uart0Initialized = 0;

// for uart 1
static volatile FifoBuffer_t  tx1Fifo;
static volatile uint8_t       tx1Data[HAL_UART_TX_FIFO_SIZE+1];

static volatile FifoBuffer_t  rx1Fifo;
static volatile uint8_t       rx1Data[HAL_UART_RX_FIFO_SIZE+1];

static volatile bool          tx1Running;
static volatile bool          udr1Empty;
static volatile bool          new1Data;
unsigned char                 uart1Initialized = 0;

/*- Implementations --------------------------------------------------------*/

bool isTx0Running(void)
{
  return tx0Running;
}

/**
 * \brief Enable the system clock to an USART module.
 *
 * This function will enable the system clock to the provided \arg usart
 * module.
 *
 * \brief usart Pointer to an USART module.
 */
static inline void usart0_enable_module_clock(void)
{
  sysclk_enable_module(POWER_RED_REG0, PRUSART0_bm);
}

static inline void usart1_enable_module_clock(void)
{
  sysclk_enable_module(POWER_RED_REG1, PRUSART1_bm);
}
/*************************************************************************//**
*****************************************************************************/
void HAL_Uart0Init(uint32_t baudrate)
{
  uint32_t fcpu = sysclk_get_cpu_hz();

  usart0_enable_module_clock();

  uint32_t brr = ((uint32_t)fcpu * 2) / (16 * baudrate) - 1;

  UBRR0 = brr;

  UCSR0A = (1 << U2X1);
  UCSR0B = (1 << TXEN1) | (1 << RXEN1) | (1 << RXCIE1);
  UCSR0C = (3 << UCSZ10);

  tx0Fifo.data  = tx0Data;
  tx0Fifo.size  = HAL_UART_TX_FIFO_SIZE;
  tx0Fifo.bytes = 0;
  tx0Fifo.head  = 0;
  tx0Fifo.tail  = 0;

  rx0Fifo.data  = rx0Data;
  rx0Fifo.size  = HAL_UART_RX_FIFO_SIZE;
  rx0Fifo.bytes = 0;
  rx0Fifo.head  = 0;
  rx0Fifo.tail  = 0;

  tx0Running  = false;
  udr0Empty   = true;
  new0Data    = false;
  uart0Initialized = 1;
}

void HAL_Uart1Init(uint32_t baudrate)
{
  uint32_t fcpu = sysclk_get_cpu_hz();

  usart1_enable_module_clock();

  uint32_t brr = ((uint32_t)fcpu * 2) / (16 * baudrate) - 1;

  UBRR1 = brr;

  UCSR1A = (1 << U2X1);
  UCSR1B = (1 << TXEN1) | (1 << RXEN1) | (1 << RXCIE1);
  UCSR1C = (3 << UCSZ10);

  tx1Fifo.data  = tx1Data;
  tx1Fifo.size  = HAL_UART_TX_FIFO_SIZE;
  tx1Fifo.bytes = 0;
  tx1Fifo.head  = 0;
  tx1Fifo.tail  = 0;

  rx1Fifo.data  = rx1Data;
  rx1Fifo.size  = HAL_UART_RX_FIFO_SIZE;
  rx1Fifo.bytes = 0;
  rx1Fifo.head  = 0;
  rx1Fifo.tail  = 0;

  tx1Running  = false;
  udr1Empty   = true;
  new1Data    = false;
  uart1Initialized = 1;
}

/*************************************************************************//**
*****************************************************************************/
uint16_t  HAL_Uart0GetWriteBuffFreePlace(void)
{
  uint16_t  freeSize;
  ATOMIC_SECTION_ENTER
  {
    freeSize = tx0Fifo.size - tx0Fifo.bytes;
  }
  ATOMIC_SECTION_LEAVE
  return freeSize;
}

/*************************************************************************//**
*****************************************************************************/
uint16_t  HAL_Uart1GetWriteBuffFreePlace(void)
{
  uint16_t  freeSize;
  ATOMIC_SECTION_ENTER
  {
    freeSize = tx1Fifo.size - tx1Fifo.bytes;
  }
  ATOMIC_SECTION_LEAVE
  return freeSize;
}

/*************************************************************************//**
*****************************************************************************/
void HAL_Uart0WriteByte(uint8_t byte)
{
//  if (uartInitialized == 0) return;

  if (tx0Fifo.bytes == tx0Fifo.size)
  {
    while (tx0Fifo.bytes == tx0Fifo.size)
    {
		  // check if interrupts are enabled
		  if ((SREG & 0x80) == 0x00)
		  {
			  // Interrupts disabled - return
        // TODO check for interrupts from UART - if disabled
			  return;
		  }
      // nothing to do - wait for the empty buffer
    }
  }
//  return;

  ATOMIC_SECTION_ENTER
  if (tx0Running == false)
  {
    UDR0 = byte;
    tx0Running = true;
    UCSR0B |= (1 << UDRIE1);
  }
  else
  {
    if (tx0Fifo.bytes <= tx0Fifo.size)
    {
      tx0Fifo.data[tx0Fifo.tail++] = byte;
      if (tx0Fifo.tail == tx0Fifo.size)
      {
        tx0Fifo.tail = 0;
      }
      tx0Fifo.bytes++;
    }
    else
    {
      // something strange happens - lose characters
    }
  }
  ATOMIC_SECTION_LEAVE
}

/*************************************************************************//**
*****************************************************************************/
void HAL_Uart1WriteByte(uint8_t byte)
{
//  if (uartInitialized == 0) return;

  if (tx1Fifo.bytes == tx1Fifo.size)
  {
    while (tx1Fifo.bytes == tx1Fifo.size)
    {
      // check if interrupts are enabled
      if ((SREG & 0x80) == 0x00)
      {
        // Interrupts disabled - return
        // TODO check for interrupts from UART - if disabled
        return;
      }
      // nothing to do - wait for the empty buffer
    }
  }
  //  return;

  ATOMIC_SECTION_ENTER
  if (tx1Running == false)
  {
    UDR1 = byte;
    tx1Running = true;
    UCSR1B |= (1 << UDRIE1);
  }
  else
  {
    if (tx1Fifo.bytes <= tx1Fifo.size)
    {
      tx1Fifo.data[tx1Fifo.tail++] = byte;
      if (tx1Fifo.tail == tx1Fifo.size)
      {
        tx1Fifo.tail = 0;
      }
      tx1Fifo.bytes++;
    }
    else
    {
      // something strange happens - lose characters
    }
  }
  ATOMIC_SECTION_LEAVE
}

/*************************************************************************//**
*****************************************************************************/
uint8_t HAL_Uart0ReadByte(void)
{
  uint8_t byte;

  if (rx0Fifo.bytes == 0)
  {
    return 0;
  }
  PRAGMA(diag_suppress=Pa082);
  ATOMIC_SECTION_ENTER
  {
    byte = rx0Fifo.data[rx0Fifo.head++];
    if (rx0Fifo.head == rx0Fifo.size)
    {
      rx0Fifo.head = 0;
    }      
    rx0Fifo.bytes--;
  }    
  ATOMIC_SECTION_LEAVE
  PRAGMA(diag_default=Pa082);

  return byte;
}

/*************************************************************************//**
*****************************************************************************/
uint8_t HAL_Uart1ReadByte(void)
{
  uint8_t byte;

  if (rx1Fifo.bytes == 0)
  {
    return 0;
  }
  PRAGMA(diag_suppress=Pa082);
  ATOMIC_SECTION_ENTER
  {
    byte = rx1Fifo.data[rx1Fifo.head++];
    if (rx1Fifo.head == rx1Fifo.size)
    {
      rx1Fifo.head = 0;
    }
    rx1Fifo.bytes--;
  }
  ATOMIC_SECTION_LEAVE
  PRAGMA(diag_default=Pa082);

  return byte;
}

/*************************************************************************//**
*****************************************************************************/
void HAL_Uart0WriteFlush(void)
{
  while (tx0Running == true);
}

/*************************************************************************//**
*****************************************************************************/
void HAL_Uart1WriteFlush(void)
{
  while (tx1Running == true);
}

/*************************************************************************//**
*****************************************************************************/
ISR(USART0_UDRE_vect)
{
  if (tx0Fifo.bytes > 0)
  {
    UDR0 = tx0Fifo.data[tx0Fifo.head++];
    if (tx0Fifo.head == tx0Fifo.size)
    {
      tx0Fifo.head = 0;
    }
    tx0Fifo.bytes--;
    tx0Running = true;
  }
  else
  {
    tx0Running = false;
    // disable interrupts
    UCSR0B &= ~(1 << UDRIE1);
  }
}

/*************************************************************************//**
*****************************************************************************/
ISR(USART1_UDRE_vect)
{
  if (tx1Fifo.bytes > 0)
  {
    UDR1 = tx1Fifo.data[tx1Fifo.head++];
    if (tx1Fifo.head == tx1Fifo.size)
    {
      tx1Fifo.head = 0;
    }
    tx1Fifo.bytes--;
    tx1Running = true;
  }
  else
  {
    tx1Running = false;
    // disable interrupts
    UCSR1B &= ~(1 << UDRIE1);
  }
}

/*************************************************************************//**
*****************************************************************************/
ISR(USART0_RX_vect)
{
  PRAGMA(diag_suppress=Pa082);

  uint8_t status = UCSR0A;
  uint8_t byte = UDR0;

  if (0 == (status & ((1 << FE1) | (1 << DOR1) | (1 << UPE1))))
  {
    if (rx0Fifo.bytes == rx0Fifo.size)
      return;

    rx0Fifo.data[rx0Fifo.tail++] = byte;
    if (rx0Fifo.tail == rx0Fifo.size)
      rx0Fifo.tail = 0;
    rx0Fifo.bytes++;

    new0Data = true;
  }

  PRAGMA(diag_default=Pa082);
}

/*************************************************************************//**
*****************************************************************************/
ISR(USART1_RX_vect)
{
  PRAGMA(diag_suppress=Pa082);

  uint8_t status = UCSR1A;
  uint8_t byte = UDR1;

  if (0 == (status & ((1 << FE1) | (1 << DOR1) | (1 << UPE1))))
  {
    if (rx1Fifo.bytes == rx1Fifo.size)
    {
      // overrun
      return;
    }

    rx1Fifo.data[rx1Fifo.tail++] = byte;
    if (rx1Fifo.tail == rx1Fifo.size)
    {
      rx1Fifo.tail = 0;
    }      
    rx1Fifo.bytes++;

    new1Data = true;
  }

  PRAGMA(diag_default=Pa082);
}

/*************************************************************************//**
*****************************************************************************/
void HAL_UartTaskHandler(void)
{
  {
    uint16_t bytes0;
    bool new0_data;

    uint16_t bytes1;
    bool new1_data;
    
    ATOMIC_SECTION_ENTER
    {
      new0_data = new0Data;
      new0Data  = false;
      bytes0    = rx0Fifo.bytes;

      new1_data = new1Data;
      new1Data  = false;
      bytes1    = rx1Fifo.bytes;
    }
    ATOMIC_SECTION_LEAVE

    if (new0_data)
    {
      HAL_Uart0BytesReceived(bytes0);
    }

    if (new1_data)
    {
      HAL_Uart1BytesReceived(bytes1);
    }
  }
}
