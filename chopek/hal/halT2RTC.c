/*
 * halT2RTC.c
 *
 *  Created on: 25 wrz 2014
 *      Author: Przemek
 */


#include "halT2RTC.h"
#include "sysclk.h"
#include "halWDT.h"

#include "trace.h"

#include <time.h>

#if defined (__GNUC__)
volatile time_t __system_time __attribute__ ((section(".noinit")));
#else
#error Unsupported compiler.
#endif

volatile uint8_t last_timer_val;
volatile uint8_t called_non_isr;

// function called from set_system_time.c
void HAL_T2RTCUpdateSysTime(void)
{
  uint8_t timer_val = TCNT2 /32;
  if (timer_val != last_timer_val)
  {
    uint8_t to_add = (timer_val) - (last_timer_val);
    __system_time += to_add;
    called_non_isr = 1;
    last_timer_val = timer_val;
  }
}


// Initializes RTC timer based on the T2 timer and external 32.768kHz oscillator
void HAL_T2RTCInit(void)
{
  sysclk_enable_peripheral_clock(&TCCR2A);

  // get reset reason
  if ((reset_cause & CHIP_RESET_CAUSE_BOD_CPU) ||
      (reset_cause & CHIP_RESET_CAUSE_POR))
	{
    // time is not correct
    __system_time = 0;
  }


  /* Set TC2 in Asynchronous mode */
  TIMSK2 =  0x00;
  ASSR   =  (1 << AS2);
  TCNT2  =  0x00;
  TCCR2A =  0x00;
  TCCR2B =  0x00;
  OCR2A  =  0x00;
  TIMSK2 |= (1 << TOIE2);

  /* divide clock by 1024 to get 8s interrupt */
  TCCR2B |= ((1 << CS20) | (1 << CS21) | (1 << CS22));

  /* wait for TCCR2B to update */
  while (ASSR & (1 << TCR2BUB)) { }
  last_timer_val = TCNT2;
  called_non_isr = 0;
}


/* Timer/Counter2 Overflow ISR */
ISR(TIMER2_OVF_vect)
{
  /* Clear Timer Overflow flag */
  TIFR2 |= (1 << TOV2);
//  /* Increment trigger count */
  HAL_T2RTCUpdateSysTime_ISR();
}

