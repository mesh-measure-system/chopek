/*
 * halRTC.h
 *
 *  Created on: 6 maj 2014
 *      Author: Przemek
 */

#ifndef HALRTC_H_
#define HALRTC_H_

#include "stdint.h"

#ifdef __cplusplus
extern "C"
{
#endif
	
#define HAL_RTC_TICKS_PER_SECOND  32768ull

void HAL_RTCInit(void);

typedef uint32_t rtc_ticks_t;

/* returns current time value
 * resolution is 32768kHz
 */
uint64_t HAL_RTCGetTickTime(void);

/* sets current time value
 * resolution is 32768kHz
 */
void HAL_RTCSetTickTime(uint64_t new_ticks);

/* returns current ticks value
 * resolution is 32768kHz
 */
rtc_ticks_t HAL_RTCGetTicks(void);

//typedef uint16_t  msTick_t;
//typedef uint64_t  time_t;
//
//typedef struct
//{
//    time_t    tv_sec;
//    msTick_t  tv_msec;
//}timeval;

//void HAL_RTCGetTime(timeval* time_p);

// delay for specified number of microseconds
//void HAL_RTCDelay(uint16_t us);

// declare compare function:
// -1 : left < right
//  0 : left = right
//  1 : left > right
int8_t HAL_RTCTickCmp(const rtc_ticks_t left, const rtc_ticks_t right);

// returns number of hw ticks for given miliSeconds interval
rtc_ticks_t HAL_RTCGetHWTicks_ms(uint32_t interval_ms);

// returns number of hw ticks for given microSeconds interval
rtc_ticks_t HAL_RTCGetHWTicks_us(uint32_t interval_us);

#ifdef __cplusplus
}
#endif

#endif /* HALRTC_H_ */
