/*
* halWDT.h
*
* Created: 2014-11-24 20:22:09
* Author: Przemek
*/

#ifndef __HAL_WDT_H__
#define __HAL_WDT_H__

#include "mega_reset_cause.h"

extern volatile reset_cause_t	reset_cause;

#endif