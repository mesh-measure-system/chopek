/*
 * halRTC.c
 *
 *  Created on: 6 maj 2014
 *      Author: Przemek
 */


#include "halRTC.h"
#include "macsc_megarf.h"
#include "halGpio.h"

#include "time.h"

#include "trace.h"

//(CPin::PIN_PORTE, 3, CPin::PIN_OUT

HAL_GPIO_PIN(ALARM, E, 5);

/* We request a tick of 1Hz */
#define CONFIG_MACSC_TIMEOUT_TICK_HZ   HAL_RTC_TICKS_PER_SECOND

int value = 0;

uint32_t  time_h;

uint32_t  rtc_clk = 0;

/**
 * \brief Symbol Counter Overflow interrupt callback function
 *
 * This function is called when an overflow interrupt has occurred on
 * Symbol Counter and toggles OVF_INT_CHK_PIN.
 */
static void rtc_mac_ovf_int_cb(void)
{
  time_h++;
}

/**
 * \brief Symbol Counter Compare 1 interrupt callback function
 *
 * This function is called when a compare match has occured on channel 1 of
 * symbol counter and toggles CMP1_INT_CHK_PIN.
 */
void example_cmp1_int_cb(void)
{
//  HAL_GPIO_ALARM_out();
  value ++;
//  value & 0x01 ? HAL_GPIO_ALARM_set() : HAL_GPIO_ALARM_clr();

//  if ((macsc_read_count() + (CONFIG_MACSC_TIMEOUT_TICK_HZ / 4)) < 0xFFFFFFFF)
//  {
//    macsc_use_cmp(MACSC_RELATIVE_CMP, macsc_read_count() + (CONFIG_MACSC_TIMEOUT_TICK_HZ), MACSC_CC1);
//  }
//  else
//  {
//    macsc_write_count(0x00000000ul);
//    macsc_use_cmp(MACSC_RELATIVE_CMP, macsc_read_count() + (CONFIG_MACSC_TIMEOUT_TICK_HZ), MACSC_CC1);
//  }
}

void HAL_RTCInit(void)
{
//  struct tm rtc_time;

//  read_rtc(&rtc_time);

//  rtc_time.tm_isdst = 0;
//
//  rtc_time.tm_year  = 2014 - 1900;
//  rtc_time.tm_mon   = 8;
//  rtc_time.tm_mday  = 18;
//  rtc_time.tm_hour  = 20;
//  rtc_time.tm_min   = 12;
//  rtc_time.tm_sec   = 23;
//
//  set_system_time( mktime(&rtc_time) );
//

  /*
   * Enable Symbol Counter and back-off slot counter
   */
  macsc_enable();
  macsc_sleep_clk_enable();
  macsc_write_clock_source(MACSC_32KHz);

  /*
   * Configure interrupts callback functions
   *
   * overflow interrupt, compare 1,2,3 and back-off slot cntr interrupts
   */
  macsc_set_ovf_int_cb(rtc_mac_ovf_int_cb);
//  macsc_set_cmp1_int_cb(example_cmp1_int_cb);
//  macsc_set_cmp2_int_cb(example_cmp2_int_cb);
//  macsc_set_cmp3_int_cb(example_cmp3_int_cb);
//  macsc_set_backoff_slot_cntr_int_cb(example_backoff_slot_cntr_int_cb);

  /*
   * Configure MACSC to generate compare interrupts from channels 1,2,3
   * Set compare mode to absolute,set compare value.
   */
//  macsc_enable_cmp_int(MACSC_CC1);
//  macsc_enable_cmp_int(MACSC_CC2);
//  macsc_enable_cmp_int(MACSC_CC3);
  macsc_enable_overflow_interrupt();
//  macsc_backoff_slot_cnt_enable();

//  macsc_use_cmp(MACSC_RELATIVE_CMP, CONFIG_MACSC_TIMEOUT_TICK_HZ, MACSC_CC1);
//  macsc_use_cmp(MACSC_RELATIVE_CMP, CONFIG_MACSC_TIMEOUT_TICK_HZ / 2, MACSC_CC2);
//  macsc_use_cmp(MACSC_RELATIVE_CMP, CONFIG_MACSC_TIMEOUT_TICK_HZ, MACSC_CC3);

}

/* returns current time value
 * resolution is 32768kHz
 */
uint64_t HAL_RTCGetTickTime(void)
{
  uint64_t value;
  ATOMIC_SECTION_ENTER
  {
    value = (uint64_t)macsc_read_count() + ((uint64_t)time_h << 32);
  }
  ATOMIC_SECTION_LEAVE
  return value;
}

/* sets current time value
 * resolution is 32768kHz
 */
void HAL_RTCSetTickTime(uint64_t new_ticks)
{
  ATOMIC_SECTION_ENTER
  {
    time_h = new_ticks >> 32;
    macsc_write_count(new_ticks);
  }
  ATOMIC_SECTION_LEAVE

//  TRACE_USER("Time set to 0x%08lx-%08lx\n", (uint32_t)(new_ticks >> 32), (uint32_t)new_ticks);

}

/* returns current ticks value
 * resolution is 32768kHz
 */
rtc_ticks_t HAL_RTCGetTicks(void)
{
  return (rtc_ticks_t)macsc_read_count();
}

// returns number of hw ticks for given miliSeconds interval
rtc_ticks_t HAL_RTCGetHWTicks_ms(uint32_t interval_ms)
{
  return ((interval_ms * HAL_RTC_TICKS_PER_SECOND) / 1000ul);
}

// returns number of hw ticks for given microSeconds interval
rtc_ticks_t HAL_RTCGetHWTicks_us(uint32_t interval_us)
{
  return ((interval_us * HAL_RTC_TICKS_PER_SECOND) / 1000000ul);
}

//void HAL_RTCGetTime(timeval* time_p)
//{
//  if (time_p != NULL)
//  {
//    uint64_t c_time = HAL_RTCGetTickTime();
//    time_p->tv_sec  = c_time >> 15;
//    time_p->tv_msec = ((((uint32_t)c_time) & 0x7ffful) * 1000ul) / HAL_RTC_TICKS_PER_SECOND;
//  }
//}
//
//void HAL_RTCSetTime(timeval* time_p)
//{
//  if (time_p != NULL)
//  {
////    uint64_t c_time = HAL_RTCGetTickTime();
////    time_p->tv_sec  = c_time >> 15;
////    time_p->tv_msec = ((((uint32_t)c_time) & 0x7ffful) * 1000ul) / HAL_RTC_TICKS_PER_SECOND;
//  }
//}

#if 0
// delay for specified number of microseconds
void HAL_RTCDelay(uint16_t us)
{
  rtc_ticks_t ticks = HAL_RTCGetHWTicks_us(us);
  if (ticks < 2)
  {
    _delay_us(us);
  }
  else
  {
    rtc_ticks_t end = ticks + HAL_RTCGetTicks();
    while (HAL_RTCTickCmp(HAL_RTCGetTicks(), end) > -1);
  }
}
#endif

// declare compare function
// -1 : rtc_clk < right
//  0 : rtc_clk = right
//  1 : rtc_clk > right
int8_t HAL_RTCTickCmp(const rtc_ticks_t rtc_clk, const rtc_ticks_t right)
{
  int8_t ret_val;
  if (((rtc_clk & 0xC0000000ul) == 0xC0000000ul) &&
       ((right & 0x80000000ul) == 0x00000000ul))
  {
    ret_val = -1;
//    TRACE_USER("compare 0x%08lx < - 0x%08lx, returns %d\n", (uint32_t)rtc_clk, (uint32_t)right, (int)ret_val);
  }
  else if (((right & 0xC0000000ul) == 0xC0000000ul) &&
            ((rtc_clk & 0x80000000ul) == 0x00000000ul))
  {
    ret_val = 1;
  }
  else
  {
    if (rtc_clk == right)
    {
      ret_val = 0;
    }
    else
    {
      if (rtc_clk < right)
      {
        ret_val = -1;
      }
      else
      {
        ret_val = 1;
      }
    }
//    TRACE_USER("compare 0x%08lx < - 0x%08lx, returns %d\n", (uint32_t)left, (uint32_t)right, (int)ret_val);
  }
  return ret_val;
}
