/*
 * halT2RTC.h
 *
 *  Created on: 25 wrz 2014
 *      Author: Przemek
 */

#ifndef HALT2RTC_H_
#define HALT2RTC_H_

#include <compiler.h>

#include "time.h"

#ifdef __cplusplus
extern "C"
{
  #endif

extern volatile time_t __system_time;
extern volatile uint8_t last_timer_val;
extern volatile uint8_t called_non_isr;

//
// Initializes RTC timer based on the T2 timer and external 32.768kHz oscillator
void      HAL_T2RTCInit(void);

// function called from set_system_time.c
void HAL_T2RTCUpdateSysTime(void);

void inline HAL_T2RTCUpdateSysTime_ISR(void)
{
  uint8_t timer_val = TCNT2 /32;
  if (called_non_isr)
  {
    uint8_t to_add = 8 - last_timer_val;
      __system_time += to_add;
  }
  else
  {
    __system_time += 8;
  }
  last_timer_val = timer_val;
  called_non_isr = 0;
}

#ifdef __cplusplus
}
#endif

#endif /* HALT2RTC_H_ */
