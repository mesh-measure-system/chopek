/*
* CNetMgr.cpp
*
* Created: 2014-11-21 09:09:29
* Author: Przemek
*/


#include "CNetMgr.h"
#include <string.h>
#include "mac.h"
#include "mac_api.h"
#include "beacon_app.h"
#include "ieee_const.h"
#include "CNetwork.h"

#include "trace.h"


CNetMgr netMgr;

void CNetMgr::Init()
{
	CRun::Init();
	ports_registered.init();
  for (uint8_t idx = 0; idx < CNET_MGR_MSDU_LEN; idx++)
  {
    msdu_list[idx] = NULL;
  }
  msdu_idx = CNET_MGR_MSDU_FIRST;
}


void CNetMgr::run()
{

}


/*
 * Callback function usr_mcps_data_conf
 *
 * @param msduHandle  Handle of MSDU handed over to MAC earlier
 * @param status      Result for requested data transmission request
 * @param Timestamp   The time, in symbols, at which the data were transmitted
 *                    (only if time stamping is enabled).
 *
 */
#if defined(ENABLE_TSTAMP)
void usr_mcps_data_conf(uint8_t msduHandle, uint8_t status, uint32_t Timestamp)
#else
void usr_mcps_data_conf(uint8_t msduHandle, uint8_t status)
#endif  /* ENABLE_TSTAMP */
{
  netMgr.confirmDelivery(msduHandle, status);
}

void CNetMgr::confirmDelivery(uint8_t msduHandle, uint8_t status)
{
  if (status != 0)
  {
	  TRACE_ERROR("usr_mcps_data_conf msduHandle = %d, status = %d\n", (uint16_t)msduHandle, (uint16_t)status);
  }

  if (msduHandle == CNET_DEFAULT_BEACON_HANDLE)
  {
    // confirmation of the back beacon frame delivery - currently nothing to do
    return;
  }
  uint8_t tmp_idx = msduHandle - CNET_MGR_MSDU_FIRST;

  if (tmp_idx < CNET_MGR_MSDU_LEN)
  {
    if (msdu_list[tmp_idx] != NULL)
    {
      msdu_list[tmp_idx]->dataDelivered(status, msdu_data_id[tmp_idx]);
      msdu_list[tmp_idx] = NULL;
    }
  }
  else
  {
    TRACE_ERROR("msduHandle out of scope (%d)\n", msduHandle);
  }
}

bool CNetMgr::SendData(CNetSendConfirm* confirm_p, int32_t data_id, uint16_t short_addr, uint8_t dstEndPoint, uint8_t srcEndPoint, uint8_t ctrl, uint8_t data_len, uint8_t* data_ptr)
{
	wpan_addr_spec_t	    dst_addr;
	uint8_t				        buffer_len;
	uint8_t				        buffer[aMaxMACPayloadSize];
  cnet_frame_header_t*  frame_header_p = (cnet_frame_header_t*)&buffer[0];

  uint8_t       tmp_idx = 0;
  // check for free ID
  for (tmp_idx = 0; tmp_idx < CNET_MGR_MSDU_LEN; tmp_idx++)
  {
    if (msdu_list[tmp_idx] == NULL)
    {
      // found
      break;
    }
    else
    {
      tmp_idx++;
    }
  }

  if (tmp_idx == CNET_MGR_MSDU_LEN)
  {
    // not found empty idx - return false
    return false;
  }

  // store confirmation class and id
  msdu_list[tmp_idx] = confirm_p;
  msdu_data_id[tmp_idx] = data_id;

  frame_header_p->dstEndPoint = dstEndPoint;
  frame_header_p->srcEndPoint = srcEndPoint;
  frame_header_p->control     = ctrl;

	if (data_len >= CNET_MAX_PAYLOAD_SIZE)
	{
		data_len = CNET_MAX_PAYLOAD_SIZE;
	}

  memcpy(&buffer[0] + sizeof(cnet_frame_header_t), data_ptr, data_len);
	buffer_len = data_len + sizeof(cnet_frame_header_t);

	/*
	 * Request transmission of indirect data to device. This will just queue this frame into the indirect data queue. Once this particular device polls for pending data, the frame will be delivered to the device.
	 */
	dst_addr.AddrMode           = WPAN_ADDRMODE_SHORT;
	dst_addr.PANId              = DEFAULT_PAN_ID;
	dst_addr.Addr.short_address = short_addr;

	if (!wpan_mcps_data_req(WPAN_ADDRMODE_SHORT, &dst_addr, buffer_len, buffer, tmp_idx + CNET_MGR_MSDU_FIRST, WPAN_TXOPT_ACK /*WPAN_TXOPT_INDIRECT_ACK*/))
	{
		TRACE_ERROR("Transmission error\n");
    return false;
	}
	return true;
}

bool CNetMgr::RegisterPort(uint8_t port_num, CNetPortReceived* port_recived)
{
	port_recived->port_num = port_num;
	// check if this port is not already registered

  // run method from classes
  CListIterator<CNetPortReceived> port_iter(&ports_registered);
  while(port_iter.get())
  {
	  if (port_iter.get()->port_num == port_num)
		{
			// port is already registered
			TRACE_ERROR("Port %d is already registered\n", (int)port_num);
			return false;
		}
	  port_iter.next();
  }
	ports_registered.add_item(port_recived);
	return true;
}

bool CNetMgr::ParseFrame(wpan_addr_spec_t *SrcAddrSpec,	wpan_addr_spec_t *DstAddrSpec, uint8_t msduLength, uint8_t *msdu,	uint8_t mpduLinkQuality, uint8_t DSN)
{
	bool frame_consumed = false;
  
  static char tmp_frame[150];

	if (msduLength >= sizeof(cnet_frame_header_t))
	{
		// check port number;
    cnet_frame_header_t*  fh_p = (cnet_frame_header_t*)msdu;
  
		uint8_t dstEndPoint = fh_p->dstEndPoint;
		uint8_t srcEndPoint = fh_p->srcEndPoint;
    uint8_t control     = fh_p->control;
    
//			TRACE_USER("dstEndPoint = %d\n", (int)dstEndPoint);

		// iterate through all registered ports
		CListIterator<CNetPortReceived> port_iter(&ports_registered);
		while(port_iter.get())
		{
//				TRACE_USER("port_iter.get()->port_num %d == dstEndPoint %d, str '%s'\n", (int)port_iter.get()->port_num, (int)dstEndPoint,  msdu + 2)
			if (port_iter.get()->port_num == dstEndPoint)
			{
				cnet_frame_data frame_data;
				frame_data.SrcAddrSpec		  = *SrcAddrSpec;
				frame_data.DstAddrSpec		  = *DstAddrSpec;
				frame_data.mpduLinkQuality	= mpduLinkQuality;
				frame_data.DSN				      = DSN;
				frame_data.dstEndPoint		  = dstEndPoint;
				frame_data.srcEndPoint		  = srcEndPoint;
        frame_data.control          = control;
				// port found
        memset(tmp_frame, 0, sizeof(tmp_frame));
        memcpy(tmp_frame, msdu + sizeof(cnet_frame_header_t), msduLength - sizeof(cnet_frame_header_t));
//        TRACE_USER("frame '%s'\n", tmp_frame);
        
				port_iter.get()->portData(&frame_data, msduLength - sizeof(cnet_frame_header_t), msdu + sizeof(cnet_frame_header_t));
				frame_consumed = true;
				break;
			}
			port_iter.next();
		}
		if (frame_consumed == false)
		{
			TRACE_ERROR("Frame not registered and not consumed\n");
		}
	}
  else
  {
    TRACE_USER("Received frame with len 0\n");
  }
	return frame_consumed;
}

/*
 * @brief Callback function usr_mcps_data_ind
 *
 * @param SrcAddrSpec      Pointer to source address specification
 * @param DstAddrSpec      Pointer to destination address specification
 * @param msduLength       Number of octets contained in MSDU
 * @param msdu             Pointer to MSDU
 * @param mpduLinkQuality  LQI measured during reception of the MPDU
 * @param DSN              DSN of the received data frame.
 * @param Timestamp        The time, in symbols, at which the data were
 * received.
 *                         (only if timestamping is enabled).
 */
#ifdef MAC_SECURITY_ZIP
void usr_mcps_data_ind(wpan_addr_spec_t *SrcAddrSpec,	wpan_addr_spec_t *DstAddrSpec,	uint8_t msduLength,	uint8_t *msdu, uint8_t mpduLinkQuality,	uint8_t DSN,

#ifdef ENABLE_TSTAMP
		uint32_t Timestamp,
#endif  /* ENABLE_TSTAMP */
		uint8_t SecurityLevel, uint8_t KeyIdMode,	uint8_t KeyIndex)

#else /* No MAC_SECURITY */
void usr_mcps_data_ind(wpan_addr_spec_t *SrcAddrSpec,	wpan_addr_spec_t *DstAddrSpec, uint8_t msduLength, uint8_t *msdu,	uint8_t mpduLinkQuality,

#ifdef ENABLE_TSTAMP
		uint8_t DSN, uint32_t Timestamp)
#else
		uint8_t DSN)
#endif  /* ENABLE_TSTAMP */
#endif
{
	netMgr.ParseFrame(SrcAddrSpec, DstAddrSpec, msduLength, msdu, mpduLinkQuality, DSN);

#ifdef ENABLE_TSTAMP
	UNUSED(Timestamp);
#endif  /* ENABLE_TSTAMP */
}

uint8_t usr_mlme_beacon_parse_ind(uint8_t* msdu_payload, uint8_t sduLength, uint8_t* msdu_beacon_response, uint8_t resp_max_len)
{
  // parse beacon content
  // TODO
  return 0;
}
