/*
 * CBattery.h
 *
 *  Created on: 12-11-2013
 *      Author: Przemek Kieszkowski
 */

#ifndef CBATTERY_H_
#define CBATTERY_H_

#include "CRun.h"
#include "sysTimer.h"
#include "CADC.h"
#include "CSettingsMgr.h"

#define BATTERY_DIVIDER_RATIO         6L
#define BATTERY_REFERENCE_VOLTAGE_MV  1600L
#define BATTERY_MAX_CONV              1024L

// class CVCC : public CADCdata
// {
//   private:
//     uint16_t  data;
//   public:
//     virtual void adcSetupChannel() {};
//     virtual void conversionFinished();
//
//     // returns battery voltage in mV
//     uint16_t getVCCVoltage();
//
//     friend class CBattery;
// };

class CBattery: public CADCdata
{
  private:
    uint16_t  data;
  public:
    virtual void adcSetupChannel() {};
    virtual void conversionFinished();

    // returns battery voltage in mV
    uint16_t getBattVoltage();

    friend class CBatteryMgr;
};

class CBatteryMgr : public CRun, public CTimerFired
{
  private:
    SYS_Timer_t batt_timer;
  public:
    enum battState_t {IDLE, START_MEASURE, BATTERY_CHANGED, MEASURED};
    battState_t state;

    CSetting<uint32_t, SETTING_UINT32> batt_measure_period;

    CBattery battery;
//     CVCC     vcc;

    void Init();
    void setState(battState_t new_state);
    virtual void run();
    virtual bool isIdle();

    virtual void timerFired(struct SYS_Timer_t* timer_fired);

    // make eclipse happy
    virtual ~CBatteryMgr() { };
};

extern CBatteryMgr battMgr;

#endif /* CBATTERY_H_ */
