/*
 * CStratificationMgr.cpp
 *
 *  Created on: 16-11-2013
 *      Author: Przemek Kieszkowski
 */

#include "CStratificationMgr.h"
#include "CCommandsMgr.h"
#include "CLedMgr.h"

#include "trace.h"

class CStratifyCommand : public CCommand
{
  private:
    bool run;
  public:
    virtual bool start_command(CStream* stream, char* argv[], int argc);
    virtual bool run_command(CStream* stream);
    virtual bool break_command(CStream* stream);
    virtual bool is_idle() {return !run;};
    virtual bool is_finished() {return !run;};

    virtual void print_help(CStream* stream) {stream->printf(F("Displays stratify value\n"));};

    virtual ~CStratifyCommand() {};
};

bool CStratifyCommand::start_command(CStream* stream, char* argv[], int argc)
{
  UNUSED(stream);
  UNUSED(argv);
  UNUSED(argc);
//  stream->printf(F("Command '%s', argc = %d\n"), argv[0], argc);
//  if (argc > 1)
//  {
//    for (int ii = 1; ii < argc; ii++)
//    {
//      stream->printf(F("    argument[%d] = '%s'\n"), ii, argv[ii]);
//    }
//  }
  stream->printf("Stratification %5.2f mm\n", stratificationMgr.stratification.getStratification());
  run = false;
  return false;
}
bool CStratifyCommand::run_command(CStream* stream)
{
  UNUSED(stream);
  return false;
}
bool CStratifyCommand::break_command(CStream* stream)
{
  UNUSED(stream);
  run = false;
  return false;
}

CStratifyCommand    stratifyCmd;
CStratificationMgr  stratificationMgr;

// returns stratification in the 0.1 um resolution
#define ST_MAX_SCALE_IN_01UM  10.000f
#define MAX_AD_RESULT         102.3f
double  CStratification::getStratification()
{
  uint32_t result;
  double converted;
  result = getConversionData();

  converted = (ST_MAX_SCALE_IN_01UM * result)/ MAX_AD_RESULT;
  return converted;
}

void CStratification::Init(ADCChannel_t chann, HAL_AdcVoltageReference_t vref)
{
  CADCdata::Init(chann, vref);
  adc_power.Init(CPin::PIN_PORTD, 4, CPin::PIN_OUT);
  adc_power.Set();
  data = 0.0f;
}

void CStratification::adcSetupChannel()
{
}

void CStratification::conversionFinished()
{
  if (data != getStratification())
  {
    data = getStratification();
//    TRACE_USER("Stratification %5.2f mm\n", data);
  }
  stratificationMgr.setState(CStratificationMgr::MEASURED);
}

void CStratificationMgr::Init()
{
  CRun::Init();
  stratification.Init(STRATIFICATION, AVCC);

  stratifyCmd.init((char*)"stratify");

  power_enable_time.Init((char*)"STR_POW_EN", 40);
  fast_measure_sleep.Init((char*)"STR_FAST_SLEEP", 330);
  fast_measure_period.Init((char*)"STR_FAST_PERIOD", 45*1000L);
  slow_measure_sleep.Init((char*)"STR_SLOW_SLEEP", 5*1000);


  // setup timer for periodic battery voltage measurement
  timer.mode = SYS_TIMER_INTERVAL_MODE;
  timer.interval = fast_measure_sleep.get();
  timer.handler =  NULL;
  timer.class_handler = this;

  pow_en_timer.mode = SYS_TIMER_INTERVAL_MODE;
  pow_en_timer.handler =  NULL;
  pow_en_timer.class_handler = this;
  timer.interval = power_enable_time.get();

  setState(IDLE);
}

void CStratificationMgr::setState(strState_t new_state)
{
  state = new_state;
  switch (new_state)
  {
    case IDLE:
    {
      timer.interval = 2*1000 + 333; // 2.333 second
      SYS_TimerStart(&timer);
      break;
    }
    case ENABLE_SENSOR_POWER:
    {
      stratification.adc_power.Clr();
      SYS_TimerStart(&pow_en_timer);
      break;
    }
    case START_MEASURE:
    {
      if (!stratification.startConversion())
      {
        TRACE_ERROR("Stratify conv start problem\n");
        state = IDLE;
      }
      break;
    }
    case MEASURED:
    {
      stratification.adc_power.Set();
      break;
    }
  }
}

void CStratificationMgr::timerFired(struct SYS_Timer_t* timer_fired)
{
  if (timer_fired == &timer)
  {
    setState(ENABLE_SENSOR_POWER);
  }
  else if (timer_fired == &pow_en_timer)
  {
    setState(START_MEASURE);
  }
}
void CStratificationMgr::run()
{
  switch (state)
  {
    case IDLE:
    case ENABLE_SENSOR_POWER:
    case START_MEASURE:
    {
      break;
    }
    case MEASURED:
    {
      setState(IDLE);
      break;
    }
  }
}

bool CStratificationMgr::isIdle()
{
  return state == IDLE;
}

