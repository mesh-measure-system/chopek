/*
 * CRun.h
 *
 *  Created on: 07-11-2013
 *      Author: Przemek Kieszkowski
 */

#ifndef CRUN_H_
#define CRUN_H_

#include "CList.h"

// This is a base class for all other classes which wants to be included into a run loop
class CRun : public CListItem<CRun>
{
  public:
    void Init();
    virtual void run() = 0;

    // this function is called by power manager module to check if all classes are in idle
    virtual bool isIdle() = 0;

    virtual ~CRun() {};
};

#endif /* CRUN_H_ */
