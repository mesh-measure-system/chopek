/*
 * CBattery.cpp
 *
 *  Created on: 12-11-2013
 *      Author: Przemek Kieszkowski
 */

#include <compiler.h>
#include "CCommandsMgr.h"
#include "CBattery.h"
#include "sysTimer.h"

#include "trace.h"

class CBattCommand : public CCommand
{
  public:
    virtual bool start_command(CStream* stream, char* argv[], int argc);
    virtual bool is_idle() {return true;};
    virtual void print_help(CStream* stream) {stream->printf(F("Displays stratify value\n"));};
    virtual ~CBattCommand() {};
};

bool CBattCommand::start_command(CStream* stream, char* argv[], int argc)
{
  UNUSED(argv);
  UNUSED(argc);
  stream->printf("Batt %d mV\n", battMgr.battery.getBattVoltage());
  return false;
}

CBattCommand    battCmd;
CBatteryMgr     battMgr;

uint16_t  CBattery::getBattVoltage()
{
  uint32_t converted;
  converted = (getConversionData() * BATTERY_REFERENCE_VOLTAGE_MV * BATTERY_DIVIDER_RATIO) / BATTERY_MAX_CONV;
  return converted;
}

void CBattery::conversionFinished()
{
  if (data != getBattVoltage())
  {
    data = getBattVoltage();
//    TRACE_USER("Battery converted %d mV\n", data);
  	battMgr.setState(CBatteryMgr::BATTERY_CHANGED);
	}
	else
	{
		battMgr.setState(CBatteryMgr::MEASURED);
	}
}

void CBatteryMgr::Init()
{
  CRun::Init();
  battery.Init(BATTERY, INTERNAL_1d6V);
//  vcc.Init(EVCC, INTERNAL_1d6V);

  battCmd.init((char*)"batery");


  batt_measure_period.Init((char*)"BATT_MEAS_PERIOD", 120*1000ul);  // 120 second

  // setup timer for periodic battery voltage measurement
  batt_timer.mode = SYS_TIMER_PERIODIC_MODE;
  batt_timer.interval = batt_measure_period.get();

  setState(IDLE);
}

void CBatteryMgr::setState(battState_t new_state)
{
  state = new_state;
  switch (new_state)
  {
    case IDLE:
    {
      SYS_TimerStart(&batt_timer, this);
      break;
    }
    case START_MEASURE:
    {
      if (!battery.startConversion() /*&& !vcc.startConversion()*/)
      {
        TRACE_ERROR("Battery conv start problem\n");
		    setState(IDLE);
      }
      break;
    }
	  case BATTERY_CHANGED:
	  {
		  uint16_t value = battery.getBattVoltage();

// TODO      logMgr.store("Batt: %d mV\n", value);
		  setState(MEASURED);
		  break;
	  }
    case MEASURED:
    {
      break;
    }
  }
}

void CBatteryMgr::timerFired(struct SYS_Timer_t* timer_fired)
{
  if (timer_fired == &batt_timer)
  {
      setState(START_MEASURE);
  }
}

void CBatteryMgr::run()
{
  switch (state)
  {
//    case IDLE:
//    case START_MEASURE:
    case MEASURED:
    {
      setState(IDLE);
      break;
    }
	  default:
	  {
		  break;
	  }
  }
}

bool CBatteryMgr::isIdle()
{
  return state == IDLE;
}
