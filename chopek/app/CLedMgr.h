/*
 * CLedMgr.h
 *
 *  Created on: 01-12-2013
 *      Author: Przemek Kieszkowski
 */

#ifndef CLEDMGR_H_
#define CLEDMGR_H_

#include "CRun.h"
#include "CLed.h"
#include "CSettingsMgr.h"
#include "sysTimer.h"

#define LEDMGR_MAX_SEQUENCE_LEN     6

class CLedSequence
{
  private:
    char  seq_name[SYS_SETTING_MAX_NAME_LENGTH + 1];
    char  seq_len_name[SYS_SETTING_MAX_NAME_LENGTH + 1];

    CSettingArray<uint16_t, SETTING_UINT16_ARRAY, LEDMGR_MAX_SEQUENCE_LEN> sequence;
    CSetting<uint32_t, SETTING_UINT32> len;

  public:
    void Init(char* seq_name, uint16_t* seq_init, uint32_t seqence_len_ms);

    friend class CLedPlay;
};

class CLedPlay : public CLed, public CTimerFired
{
  private:
    SYS_Timer_t play_timer;
    SYS_Timer_t stop_timer;

    CLedSequence* seqence;
    uint8_t       next_sequence_idx;

  public:
    void play_handler(SYS_Timer_t *timer);
    void stop_handler(SYS_Timer_t *timer);

    void Play(CLedSequence* seq_p);
    void Stop();

    virtual void timerFired(struct SYS_Timer_t* timer);
};

class CLedMgr : public CRun
{
  private:
    CPin      pinGreen;
    CPin      pinBlue;
    CPin      pinRed;

  public:

//    CLed  ledGreen;
//    CLed  ledBlue;
//    CLed  ledRed;

    CLedPlay  ledGreen;
    CLedPlay  ledBlue;
    CLedPlay  ledRed;

    CLedSequence  powerSq;
    CLedSequence  alarmSq;

    void Init();
    virtual void run();
    virtual bool isIdle();

    virtual ~CLedMgr() {};
};

extern CLedMgr ledMgr;

#endif /* CLEDMGR_H_ */
