/*
 * CStream.h
 *
 *  Created on: 11-12-2013
 *      Author: Przemek Kieszkowski
 */

#ifndef CSTREAM_H_
#define CSTREAM_H_

#include "CPrint.h"

class CStream : public CPrint
{
  protected:
    bool    breakStream;
    bool    endStream;
    bool    asciiQP;

  public:
    virtual uint16_t getRemainingOutBuffSize() = 0;
    virtual bool isOutBuffEmpty();

    virtual bool flushStream() = 0;
    virtual void sendBreakStream() {breakStream = true; };
    virtual void sendEndStream() {endStream = true; };

    virtual ~CStream() {};
};

#endif /* CSTREAM_H_ */
