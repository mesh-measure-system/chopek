/*
 * CNetCmd.h
 *
 *  Created on: 13 sty 2014
 *      Author: Przemek Kieszkowski
 */

#ifndef CNETCMD_H_
#define CNETCMD_H_

#include "CCommandsMgr.h"
#include "CNetMgr.h"


class CNetCmd : public CCommand, public CNetSendConfirm
{
  public:
    virtual bool start_command(CStream* stream, char* argv[], int argc);

    virtual void print_help(CStream* stream) {stream->printf(F("sends command over lwmesh connection\n"));};

    virtual void dataDelivered(uint8_t status, int32_t data_id);

    virtual ~CNetCmd() {};
};

extern CNetCmd netCmd;

#endif /* CNETCMD_H_ */
