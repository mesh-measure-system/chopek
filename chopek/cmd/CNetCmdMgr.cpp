/*
 * CNetCmdMgr.cpp
 *
 *  Created on: 8 lut 2014
 *      Author: Przemek Kieszkowski
 */

#include <string.h>

#include "CNetCmdMgr.h"
#include "CCommandsMgr.h"
#include "halUart.h"
#include "CNetwork.h"

#include "trace.h"

CNetCmdMgr netCmdMgr;

class CNetResponse:  public CNetPortReceived
{
	virtual void portData(cnet_frame_data* frame_data_ptr, uint8_t data_len, uint8_t* data_ptr)
	{
		// received response from network command
    static char net_cmd_resp[CNET_CMD_MAX_CMD_LEN + 1];
    
    // copy command from network
    memcpy(net_cmd_resp, data_ptr, data_len);
    net_cmd_resp[data_len] = 0;

    // send response to the last communication channel
    cmdMgr.getOutStream()->printf("fn -a%u -c0x%02x -l%u '%s'\n", frame_data_ptr->SrcAddrSpec.Addr.short_address, frame_data_ptr->control, (int)data_len, net_cmd_resp);
//    cmdMgr.getOutStream()->write((char*)data_ptr, data_len);
    cmdMgr.getOutStream()->flushStream();
		// 			NWK_SetAckControl(NET_COMMAND_ACK_OK);
	}
};

CNetResponse netRes;

class CNetBeaconResponse:  public CNetPortReceived
{
  virtual void portData(cnet_frame_data* frame_data_ptr, uint8_t data_len, uint8_t* data_ptr)
  {
    // received response from beacon network command
    data_ptr[data_len] = 0;
//    TRACE_USER("Received beacon resp '%s\n", data_ptr);
  }
};

CNetBeaconResponse netBeaconRes;

void CNetCmdMgr::Init()
{
  CRun::Init();

  out_buff_len    = 0;
  net_data_available = false;

  wait_to_send_confirm = false;

  // register for the network socket to get command
  netMgr.RegisterPort(NET_COMMAND_ENDPOINT_ID, this);
  netMgr.RegisterPort(NET_COMMAND_RESPONSE_ENDPOINT_ID, &netRes);
  netMgr.RegisterPort(CNET_DEFAULT_BEACON_ENDPOINT_ID, &netBeaconRes);
}

void CNetCmdMgr::portData(cnet_frame_data* frame_data_ptr, uint8_t data_len, uint8_t* data_ptr)
{
	switch (frame_data_ptr->dstEndPoint)
	{
		case NET_COMMAND_ENDPOINT_ID:
		{
			if (!net_data_available)
			{
				netCmdMgr.net_data_available = true;
				frame_info = *frame_data_ptr;

				if (data_len > sizeof(new_net_data))
				{
          TRACE_ERROR("Data len is to long %d > %d\n", data_len, sizeof(new_net_data));
					data_len = sizeof(new_net_data);
				}
				memcpy(&new_net_data, data_ptr, data_len);
				new_net_data_len = data_len;

				// TODO command copied - send ack
				//NWK_SetAckControl(NET_COMMAND_ACK_OK);
			}
			else
			{
				// cannot parse command - I am busy
				//		NWK_SetAckControl(NET_COMMAND_ACK_BUSY);
			}
			break;
		}
	}
}

void CNetCmdMgr::run()
{
  if (net_data_available)
  {
    char net_cmd[CNET_CMD_MAX_CMD_LEN + 1];
    // process the frame
    // copy command from network
    memcpy(net_cmd, new_net_data, new_net_data_len);
    net_cmd[new_net_data_len] = 0;

    TRACE_USER("Received command len = %d, %s\n", new_net_data_len, net_cmd);
    out_buff_len = 0;

    // function should be called to parse commands
    if (cmdMgr.exec_commands(net_cmd, this))
    {
      // command parsed successfully
      if (out_buff_len > 0)
      {
        // sending back command results
        flushStream();
      }
      // after completing command, print a new prompt
    }
    else
    {
      TRACE_ERROR("Command execution error\n");
      // TODO send back error
    }
    net_data_available = false;
  }
}

bool CNetCmdMgr::isIdle()
{
  return !net_data_available;
}

char* CNetCmdMgr::getPrintfBuffer()
{
  return print_buff;
}

int   CNetCmdMgr::getPrintfBufferSize()
{
  return sizeof(print_buff);
}

void  CNetCmdMgr::write(char c)
{
  if (out_buff_len < sizeof(out_buff))
  {
    out_buff[out_buff_len] = c;
    out_buff_len++;
  }
}

uint16_t CNetCmdMgr::getRemainingOutBuffSize()
{
  return sizeof(out_buff) - out_buff_len;
}

bool CNetCmdMgr::isOutBuffEmpty()
{
  if ((out_buff_len == 0) &&
      (wait_to_send_confirm == false))
  {
    return true;
  }
  return false;
}

void CNetCmdMgr::dataDelivered(uint8_t status, int32_t data_id)
{
  if (status == 0)
  {
    wait_to_send_confirm = false;
    out_buff_len = 0;
    endStream = false;
    breakStream = false;
  }
  else
  {
    uint8_t ctrl = (endStream ? CNET_FH_CTRL_END_COMMAND : 0) | (breakStream ? CNET_FH_CTRL_BREAK_COMMAND : 0) | (asciiQP ? CNET_FH_CTRL_ASCII_COMMAND : 0);
    // error status - resend command
    netMgr.SendData(this, 1, frame_info.SrcAddrSpec.Addr.short_address, frame_info.srcEndPoint, NET_COMMAND_ENDPOINT_ID, ctrl, out_buff_len, (uint8_t*)out_buff);
  }
}

bool CNetCmdMgr::flushStream()
{
  if (wait_to_send_confirm)
  {
    return false;
  }

  uint8_t ctrl = (endStream ? CNET_FH_CTRL_END_COMMAND : 0) | (breakStream ? CNET_FH_CTRL_BREAK_COMMAND : 0) | (asciiQP ? CNET_FH_CTRL_ASCII_COMMAND : 0);
  wait_to_send_confirm = true;
  netMgr.SendData(this, 1, frame_info.SrcAddrSpec.Addr.short_address, frame_info.srcEndPoint, NET_COMMAND_ENDPOINT_ID, ctrl, out_buff_len, (uint8_t*)out_buff);
  return true;
}

