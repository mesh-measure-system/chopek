/*
 * CArgParser.h
 *
 *  Created on: 28-12-2013
 *      Author: Przemek Kieszkowski
 */

#ifndef CARGPARSER_H_
#define CARGPARSER_H_

#include "CFlashString.h"

#define CARG_SWITCH_SIGN  '-'
#define CARG_MAX_ARGS_LEN 20

class CArgParser
{
  private:
    char** argv_c;
    int   argc_c;
    char* args_c[CARG_MAX_ARGS_LEN];
    char* current_sw_arg_p;

    int curr_argc;
    int curr_argc_idx;
    int curr_switch;


    enum {
      CARG_SWITCH,
      CARG_SWITCH_WITH_ARGUMENT,
      CARG_SWITCH_INVALID,
    };
    int parseSwitch(char sw);
  public:
    int not_switch_idx;

    enum {
      CARG_END_OF_SWITCH = -1,
      CARG_INVALID_ARG = -2,
      CARG_NO_ARG_PARAM_FOUND = -3,
    };
    CArgParser(char* argv[], int argc, const __FlashStringHelper* args);
    int   getSwitch();
    char* getSwitchArgument();
    long int getSwitchArgumentLong();
    virtual ~CArgParser() {};
};

#endif /* CARGPARSER_H_ */
