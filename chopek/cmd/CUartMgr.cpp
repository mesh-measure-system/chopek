/*
 * CUartMgr.cpp
 *
 *  Created on: 26-11-2013
 *      Author: Przemek Kieszkowski
 */

#include "CUartMgr.h"
#include "halUart.h"
#include "CCommandsMgr.h"

#include "trace.h"

CUartMgr uartMgr;

void HAL_Uart0BytesReceived(uint16_t bytes)
{
  uartMgr.uartBytesReceived(bytes);
}

void uartTimerHandler(SYS_Timer_t *timer)
{
  UNUSED(timer);

  switch (uartMgr.state)
  {
    case CUartMgr::UART_SLEEP:
    {
      uartMgr.startCheckForActiveState();
      break;
    }
    case CUartMgr::UART_CHECK_FOR_ACTIVE:
    {
      uartMgr.startSleepState();
      break;
    }
    case CUartMgr::UART_ACTIVE:
    {
      uartMgr.startSleepState();
      break;
    }
    default:
    {
      // do nothing
    }
  }
}

void CUartMgr::Init()
{
  CRun::Init();

  bytes_received    = false;
  uart_cmd_len      = 0;
  uart_out_buff_len = 0;

  uartSleepEnable.Init((char*)"uartSleepEnable", 0);
  uartSleep.Init((char*)"uartSleep", 60*1000L);
  uartCheckForActive.Init((char*)"uartCheckActive", 3*1000L);
  uartActive.Init((char*)"uartActive", 60*1000L);

  if (uartSleepEnable.get())
  {
    startCheckForActiveState();
  }
  else
  {
    startAlwaysActiveState();
  }
}

char* CUartMgr::getPrintfBuffer()
{
  return print_buff;
}

int   CUartMgr::getPrintfBufferSize()
{
  return sizeof(print_buff);
}

void  CUartMgr::write(char c)
{
  if (uart_out_buff_len < sizeof(uart_out_buff))
  {
    uart_out_buff[uart_out_buff_len] = c;
    uart_out_buff_len++;
  }
}

uint16_t  CUartMgr::getRemainingOutBuffSize()
{
  return sizeof(uart_out_buff) - uart_out_buff_len;
}

bool CUartMgr::isOutBuffEmpty()
{
  if (uart_out_buff_len == 0)
  {
    return true;
  }
  return false;
}

bool CUartMgr::flushStream()
{
  uint8_t idx = 0;
  // command parsed successfully
  while (idx < uart_out_buff_len)
  {
    // sending back command results
    HAL_Uart0WriteByte(uart_out_buff[idx]);
    idx++;
  }
  uart_out_buff_len = 0;
  return true;
}

void CUartMgr::startAlwaysActiveState()
{
  state = UART_ALWAYS_ACTIVE;
}

void CUartMgr::startSleepState()
{
  SYS_TimerStop(&uart_timer);
  // setup timer
  uart_timer.mode = SYS_TIMER_INTERVAL_MODE;
  uart_timer.interval = uartSleep.get();
  uart_timer.handler =  uartTimerHandler;
  SYS_TimerStart(&uart_timer);
  if (state != UART_SLEEP)
  {
    // end prompt
    TRACE_USER("Start sleep\n");
  }
  state = UART_SLEEP;
}

void CUartMgr::startCheckForActiveState()
{
  SYS_TimerStop(&uart_timer);
  // setup timer
  uart_timer.mode = SYS_TIMER_INTERVAL_MODE;
  uart_timer.interval = uartCheckForActive.get();
  uart_timer.handler =  uartTimerHandler;
  SYS_TimerStart(&uart_timer);
  if (state != UART_CHECK_FOR_ACTIVE)
  {
    // start prompt
    TRACE_USER("Start active\n");
  }
  state = UART_CHECK_FOR_ACTIVE;
}

void CUartMgr::startActiveState()
{
  SYS_TimerStop(&uart_timer);
  // setup timer
  uart_timer.mode = SYS_TIMER_INTERVAL_MODE;
  uart_timer.interval = uartActive.get();
  uart_timer.handler =  uartTimerHandler;
  SYS_TimerStart(&uart_timer);
  state = UART_ACTIVE;
}

void CUartMgr::run()
{
  if (state == UART_ALWAYS_ACTIVE)
  {
    // uart is always in active state
  }
  else if (bytes_received)
  {
    bytes_received = false;
    switch (state)
    {
      case UART_SLEEP:
      case UART_ACTIVE:
      case UART_CHECK_FOR_ACTIVE:
      {
        startActiveState();
        break;
      }
      default:
      {
        // do nothing
      }
    }
  }
}

bool CUartMgr::isIdle()
{
  return state == UART_SLEEP;
}

void CUartMgr::printPrompt(void)
{
//  timeOfLastActivity = millis();
//  client_p->flush();
  uart_cmd_len = 0; //count of characters received
  printf(">");
}

void CUartMgr::printErrorMessage()
{
  printf("Unrecognized command.  ? for help.\n");
}

void CUartMgr::uartBytesReceived(uint16_t bytes)
{
  uint8_t c = 0;

  bytes_received = true;

  // copy waiting characters into textBuff
  //until textBuff full, CR received, or no more characters
  do
  {
    c = HAL_Uart0ReadByte();
    if (!((c == 0x0d) ||
         (c == 0x0a)))
    {
      uart_cmd[uart_cmd_len] = c;
      uart_cmd_len++;
    }
    bytes--;
  }
  while (uart_cmd_len <= CUART_CMD_MAX_CMD_LEN && c != 0x0d && bytes > 0);

  //if CR found go look at received text and execute command
  if ((c == 0x0d) || (c == 0x0a))
  {
    uart_cmd[uart_cmd_len] = 0;
    // function should be called to parse commands
    if (cmdMgr.exec_commands(uart_cmd, this))
    {
      flushStream();
      // after completing command, print a new prompt
      printPrompt();
    }
    else
    {
      printErrorMessage();
      printPrompt();
    }
  }

  // if textBuff full without reaching a CR, print an error message
  if(uart_cmd_len >= CUART_CMD_MAX_CMD_LEN)
  {
    printf("\n");
    printErrorMessage();
    printPrompt();
  }
  // if textBuff not full and no CR, do nothing else;
  // go back to loop until more characters are received
}

