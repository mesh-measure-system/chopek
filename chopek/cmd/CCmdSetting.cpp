/*
 * CCmdSetting.cpp
 *
 *  Created on: 04-01-2014
 *      Author: Przemek Kieszkowski
 */

#include "CCmdSetting.h"
#include "CArgParser.h"
#include "sysSettings.h"

#include "string.h"
#include <stdlib.h>


#include "trace.h"

#define SETT_ARR_MAX_SIZE 30


CCmdSettingList   cmdSettingList;
CCmdSettingSet    cmdSettingSet;

bool CCmdSettingSet::break_command(CStream* stream)
{
  UNUSED(stream);
  return false;
}

bool CCmdSettingSet::start_command(CStream* stream, char* argv[], int argc)
{
  // no additional options for set setting
  CArgParser  argP(argv, argc, F(""));
  int         c;
  bool finish = false;
  int sw_idx;
  UNUSED(stream);

  while (!finish)
  {
    c = argP.getSwitch();
    switch (c)
    {
      case CArgParser::CARG_INVALID_ARG:
      default:
      {
        break;
      }
      case CArgParser::CARG_END_OF_SWITCH:
      {
        finish = true;
        break;
      }
    }
  }

  // first argument should be setting name - and then setting value(s)
  sw_idx = 0;
  while (sw_idx < argP.not_switch_idx)
  {
    char* setting_name = argv[sw_idx];
    setting_idx_t setting;

    if (SYS_FindSetting(setting_name, &setting))
    {
      // setting found
      TRACE_USER("Setting %s type = %d\n", setting_name, setting.sett_struct.setting_type);
      switch (setting.sett_struct.setting_type)
      {
        case SYS_SETTING_STRING:
        {
          if (++sw_idx < argP.not_switch_idx)
          {
            SYS_SettingSetString(&setting, argv[sw_idx]);
          }
          break;
        }
        case SYS_SETTING_UINT8:
        {
          if (++sw_idx < argP.not_switch_idx)
          {
            SYS_SettingSetUInt8(&setting, strtol(argv[sw_idx], NULL, 0));
          }
          break;
        }
        case SYS_SETTING_UINT16:
        {
          if (++sw_idx < argP.not_switch_idx)
          {
            SYS_SettingSetUInt16(&setting, strtol(argv[sw_idx], NULL, 0));
          }
          break;
        }
        case SYS_SETTING_UINT32:
        {
          if (++sw_idx < argP.not_switch_idx)
          {
            SYS_SettingSetUInt32(&setting, strtol(argv[sw_idx], NULL, 0));
          }
          break;
        }
        case SYS_SETTING_UINT8_ARRAY:
        {
          uint8_t arr[SETT_ARR_MAX_SIZE];
          uint8_t idx = 0;
          memset(arr, 0, sizeof(arr));

          while ((++sw_idx < argP.not_switch_idx) &&
              ((sizeof(arr[0]) * idx) < setting.sett_struct.value_len))
          {
            arr[idx++] = strtol(argv[sw_idx], NULL, 0);
          }
          SYS_SettingSetValue(&setting, arr, sizeof(arr[0]) * idx);
          break;
        }
        case SYS_SETTING_UINT16_ARRAY:
        {
          uint16_t  arr[SETT_ARR_MAX_SIZE];
          uint8_t   idx = 0;
          memset(arr, 0, sizeof(arr));

          while ((++sw_idx < argP.not_switch_idx) &&
              ((sizeof(arr[0]) * idx) < setting.sett_struct.value_len))
          {
            arr[idx++] = strtol(argv[sw_idx], NULL, 0);
          }
          SYS_SettingSetValue(&setting, (uint8_t*)arr, sizeof(arr[0]) * idx);
          break;
        }
        case SYS_SETTING_UINT32_ARRAY:
        {
          uint32_t  arr[SETT_ARR_MAX_SIZE];
          uint8_t   idx = 0;
          memset(arr, 0, sizeof(arr));

          while ((++sw_idx < argP.not_switch_idx) &&
              ((sizeof(arr[0]) * idx) < setting.sett_struct.value_len))
          {
            arr[idx++] = strtol(argv[sw_idx], NULL, 0);
          }
          SYS_SettingSetValue(&setting, (uint8_t*)arr, sizeof(arr[0]) * idx);
          break;
        }
      }
    }
    sw_idx++;
  }
  return false;
}

bool CCmdSettingSet::run_command(CStream* stream)
{
  UNUSED(stream);
  return false;
}

bool CCmdSettingList::start_command(CStream* stream, char* argv[], int argc)
{
  UNUSED(stream);
  UNUSED(argv);
  UNUSED(argc);

  // currently we do not support any options for this command

  prev_idx.sett_addr = SYS_SETTING_INVALID;
  finished = false;
  return false;
}

bool CCmdSettingList::is_finished()
{
  return finished;
};

bool CCmdSettingList::run_command(CStream* stream)
{
  if (stream->isOutBuffEmpty() == false)
  {
    // wait for empty buffer
    return true;
  }

  if (SYS_SettingNext(&prev_idx) != NULL)
  {
    char name[SYS_SETTING_MAX_NAME_LENGTH + 1];
    char value[SYS_SETTING_MAX_VALUE_LENGTH + 1];
    SYS_SettingGetName(&prev_idx, name, sizeof(name));
    stream->printf(name);
    switch(prev_idx.sett_struct.setting_type)
    {
      case SYS_SETTING_STRING:
      {
        SYS_SettingGetValue(&prev_idx, (uint8_t*)value, sizeof(value));
        stream->printf(F("[S]:%s\n"), value);
        break;
      }
      case SYS_SETTING_UINT8:
      {
        uint8_t val8;
        SYS_SettingGetValue(&prev_idx, (uint8_t*)&val8, sizeof(val8));
        stream->printf(F("[U8]:%u\n"), (unsigned int)val8);
        break;
      }
      case SYS_SETTING_UINT16:
      {
        uint16_t val16;
        SYS_SettingGetValue(&prev_idx, (uint8_t*)&val16, sizeof(val16));
        stream->printf(F("[U16]:%u\n"), (unsigned int)val16);
        break;
      }
      case SYS_SETTING_UINT32:
      {
        uint32_t val32;
        SYS_SettingGetValue(&prev_idx, (uint8_t*)&val32, sizeof(val32));
        stream->printf(F("[U32]:%lu\n"), (uint32_t)val32);
        break;
      }
      case SYS_SETTING_UINT8_ARRAY:
      {
        uint8_t   val8[32];
        uint8_t   idx;
        stream->printf(F("[U8[%d]]:"), (int)(prev_idx.sett_struct.value_len / sizeof(val8[0])));

        SYS_SettingGetValue(&prev_idx, (uint8_t*)val8, sizeof(val8));
        for (idx = 0; idx < (prev_idx.sett_struct.value_len / sizeof(val8[0])); idx++)
        {
          if (idx < sizeof(val8)/sizeof(val8[0]))
          {
            stream->printf(F("%u,"), (int)val8[idx]);
          }
          else
          {
            stream->printf(F("*,"));
          }
        }
        stream->printf(F("\n"));
        break;
      }
      case SYS_SETTING_UINT16_ARRAY:
      {
        uint16_t  val16[16];
        uint8_t   idx;

        stream->printf(F("[U16[%d]]:"), (int)(prev_idx.sett_struct.value_len / sizeof(val16[0])));

        SYS_SettingGetValue(&prev_idx, (uint8_t*)val16, sizeof(val16));
        for (idx = 0; idx < (prev_idx.sett_struct.value_len / sizeof(val16[0])); idx++)
        {
          if (idx < sizeof(val16)/sizeof(val16[0]))
          {
            stream->printf(F("%u,"), (int)val16[idx]);
          }
          else
          {
            stream->printf(F("*,"));
          }
        }
        stream->printf(F("\n"));
        break;
      }
      case SYS_SETTING_UINT32_ARRAY:
      {
        uint32_t  val32[16];
        uint8_t   idx;

        stream->printf(F("[U32[%d]]:"), (int)(prev_idx.sett_struct.value_len / sizeof(val32[0])));

        SYS_SettingGetValue(&prev_idx, (uint8_t*)val32, sizeof(val32));
        for (idx = 0; idx < (prev_idx.sett_struct.value_len / sizeof(val32[0])); idx++)
        {
          if (idx < sizeof(val32)/sizeof(val32[0]))
          {
            stream->printf(F("%u,"), (int)val32[idx]);
          }
          else
          {
            stream->printf(F("*,"));
          }
        }
        stream->printf(F("\n"));
        break;
      }

      default:
      {
        stream->printf(F("[?]:\n"));
        break;
      }
    }
  }
  else
  {
    finished = true;
  }
  return false;
}

bool CCmdSettingList::break_command(CStream* stream)
{
  UNUSED(stream);
  finished = true;
  return false;
}
