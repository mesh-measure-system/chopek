/*
 * CPrint.cpp
 *
 *  Created on: 11-12-2013
 *      Author: Przemek Kieszkowski
 */

#include <stdio.h>
#include <stdarg.h>

#include "CPrint.h"


int CPrint::printf(const char* format, ...)
{
  int written;
  va_list args;
  va_start (args, format);
  written = vsnprintf(getPrintfBuffer(), getPrintfBufferSize(), format, args);
  write(getPrintfBuffer(), written);
  va_end (args);
  return written;
}

int CPrint::printf(const __FlashStringHelper* format, ...)
{
  int written;
  va_list args;
  va_start (args, format);
  written = vsnprintf_P(getPrintfBuffer(), getPrintfBufferSize(), (const char *)format, args);
  write(getPrintfBuffer(), written);
  va_end (args);
  return written;
}


void CPrint::write(char* buff, int size)
{
  while (size--)
  {
    write(*buff);
    buff++;
  }
}
