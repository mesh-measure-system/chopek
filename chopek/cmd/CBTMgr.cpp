/* 
* CBTMgr.cpp
*
* Created: 2015-03-01 18:35:00
* Author: przemek
*/


#include "CBTMgr.h"
#include "CCommandsMgr.h"
#include "CArgParser.h"
#include "hal.h"
#include "string.h"

#include "trace.h"

CBTMgr BTMgr;

void HAL_Uart1BytesReceived(uint16_t bytes)
{
  BTMgr.uartBytesReceived(bytes);
}

class CBTCommand : public CCommand
{
  public:
  virtual bool start_command(CStream* stream, char* argv[], int argc)
  {
      // no additional options for set setting
      CArgParser  argP(argv, argc, F("b"));
      int         c;
      bool        finish = false;
      bool        break_set = 0;
	  
      while (!finish)
      {
        c = argP.getSwitch();
        switch (c)
        {
          case 'b':
          {
            break_set = true;
            break;
          }
          case CArgParser::CARG_INVALID_ARG:
          default:
          {
            break;
          }
          case CArgParser::CARG_END_OF_SWITCH:
          {
            finish = true;
            break;
          }
        }
      }

      if ((argP.not_switch_idx > 0))
      {
        TRACE_USER("Send '%s'\n", argv[0]);
    	  if (break_set)
        {
          BTMgr.sendATCommand(argv[0]);
          BTMgr.sendATCommand((char*)"\r");
        }
        else
        {
          BTMgr.printf("%s\r", argv[0]);
          BTMgr.flushStream();
        }
      }
      else
      {
        stream->printf(F("Not proper command params\n"));
      }
      return false;
  };
  virtual bool is_idle() {return true;};
  virtual bool is_finished() {return true;};

  virtual void print_help(CStream* stream) {stream->printf("Sends command to bluetooth adapter: bt 'command to send'\n");};

  virtual ~CBTCommand() {};
};

CBTCommand cmdBT;


void CBTMgr::Init()
{
  CRun::Init();

  out_buff_len    = 0;

  HAL_Uart1Init(19200);
  
  pinReset.Init(CPin::PIN_PORTD, 5, CPin::PIN_OUT);;
  pinRTS.Init(CPin::PIN_PORTE, 6, CPin::PIN_IN);
  pinCTS.Init(CPin::PIN_PORTD, 7, CPin::PIN_OUT);
  
  pinRTS.Pullup();
  
  pinReset.Clr();
  pinCTS.Clr();
  HAL_Delay(1000);
  pinReset.Set();

  bytes_received  = false;
  cmd_len      = 0;
  
  mode = CBT_MODE_INIT;

  cmdBT.init((char*)"bt");

}

void CBTMgr::timerFired(struct SYS_Timer_t* timer)
{
  
}

void CBTMgr::run()
{
  switch (mode)
  {
    case CBT_MODE_INIT:
    {
//      TRACE_USER("Switch BT device to command mode\n");
      break;
    }
    
    case CBT_MODE_COMMAND:
    {
      break;
    }
    
    case CBT_MODE_TRANSMIT:
    {
      break;
    }
  }
}

bool CBTMgr::isIdle()
{
  return false;
}

char* CBTMgr::getPrintfBuffer()
{
  return print_buff;
}

int   CBTMgr::getPrintfBufferSize()
{
  return sizeof(print_buff);
}

void  CBTMgr::write(char c)
{
  if (out_buff_len < sizeof(out_buff))
  {
    out_buff[out_buff_len] = c;
    out_buff_len++;
  }
}

uint16_t  CBTMgr::getRemainingOutBuffSize()
{
  return sizeof(out_buff) - out_buff_len;
}

bool CBTMgr::isOutBuffEmpty()
{
  if (out_buff_len == 0)
  {
    return true;
  }
  return false;
}

void CBTMgr::printPrompt(void)
{
//  printf(">");
}

void CBTMgr::printErrorMessage()
{
  printf("Unrecognized command.  ? for help.\n");
}


void CBTMgr::uartBytesReceived(uint16_t bytes)
{
  uint8_t c = 0;

  bytes_received = true;

  // copy waiting characters into textBuff
  //until textBuff full, CR received, or no more characters
  do
  {
    c = HAL_Uart1ReadByte();
    if (!((c == 0x0d) || (c == 0x0a)))
    {
      cmd[cmd_len] = c;
      cmd_len++;
    }
    bytes--;
  }
  while (cmd_len <= CBT_CMD_MAX_CMD_LEN && c != 0x0d && bytes > 0);

  //if CR found go look at received text and execute command
  if ((c == 0x0d) || (c == 0x0a))
  {
    cmd[cmd_len] = 0;
    // function should be called to parse commands
	  if (strlen(cmd) > 0)
	  {	
      TRACE_USER("Command '%s'\n", cmd);
		  if (cmdMgr.exec_commands(cmd, this))
		  {
		    flushStream();
		    // after completing command, print a new prompt
		    printPrompt();
		  }
		  else
		  {
  		  printErrorMessage();
	  	  printPrompt();
		  }
	  }
	  else
	  {
		  // nothing to do
	  }
    cmd_len = 0; //count of characters received
  }

  // if textBuff full without reaching a CR, print an error message
  if (cmd_len >= CBT_CMD_MAX_CMD_LEN)
  {
    printf("\n");
    printErrorMessage();
    printPrompt();
  }
  // if textBuff not full and no CR, do nothing else;
  // go back to loop until more characters are received
}

bool CBTMgr::flushStream()
{
  uint8_t idx = 0;
  out_buff[out_buff_len] = 0;
  // command parsed successfully
  while (idx < out_buff_len)
  {
    // sending back command results
    HAL_Uart1WriteByte(out_buff[idx]);
    idx++;
  }
  out_buff_len = 0;
  return true;
}

void CBTMgr::sendATCommand(char* at_command)
{
  uint8_t at_cmd_len = strlen(at_command);
  flushStream();
  HAL_Delay(25 * 1000);
  
  for (uint8_t idx = 0; idx < at_cmd_len; idx++)
  {
    HAL_Uart1WriteByte(at_command[idx]);
    HAL_Delay(25 * 1000);
  }
}
