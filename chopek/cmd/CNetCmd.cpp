/*
 * CNetCmd.cpp
 *
 *  Created on: 13 sty 2014
 *      Author: Przemek Kieszkowski
 */

#include "string.h"
#include "mac_api.h"
#include "beacon_app.h"
#include "ieee_const.h"

#include <CNetCmd.h>
#include "CNetCmdMgr.h"
#include "CArgParser.h"

#include "trace.h"

CNetCmd netCmd;

void CNetCmd::dataDelivered(uint8_t status, int32_t data_id)
{
  TRACE_USER("Cmd delivered status %d\n", (int)status);
}

bool CNetCmd::start_command(CStream* stream, char* argv[], int argc)
{
  // no additional options for set setting
  CArgParser  argP(argv, argc, F("a:"));
  int         c;
  bool        finish = false;
  uint16_t    address = 0;
  bool        address_set = false;

  while (!finish)
  {
    c = argP.getSwitch();
    switch (c)
    {
      case 'a':
      {
        // getting opt argument
        address = argP.getSwitchArgumentLong();
        address_set = true;
        break;
      }
      case CArgParser::CARG_INVALID_ARG:
      default:
      {
        break;
      }
      case CArgParser::CARG_END_OF_SWITCH:
      {
        finish = true;
        break;
      }
    }
  }

  if (address_set && (argP.not_switch_idx > 0))
  {
    stream->printf(F("sending command to 0x%04x '%s'\n"), address, argv[0]);

	  if (!netMgr.SendData(this, 1, address, NET_COMMAND_ENDPOINT_ID, NET_COMMAND_RESPONSE_ENDPOINT_ID, CNET_FH_CTRL_END_COMMAND | CNET_FH_CTRL_ASCII_COMMAND, strlen(argv[0]), (uint8_t*)argv[0]))
	  {
		  TRACE_ERROR("Transmission error\n");
	  }
  }
  else
  {
    stream->printf(F("Not proper command params\n"));
  }
  return false;
}
