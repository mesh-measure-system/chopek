/*
 * CCommandsMgr.cpp
 *
 *  Created on: 27-11-2013
 *      Author: Przemek Kieszkowski
 */

#include <string.h>
#include "CCommandsMgr.h"
#include "CArgParser.h"
#include "CFlashString.h"

#include "trace.h"

bool CCommand::operator== (char* cmd_name)
{
  return strcmp(this->command_name, cmd_name) == 0;
}

void CCommand::init(char* cmd_name)
{
  this->command_name = cmd_name;
  cmdMgr.registerCommand(this);
}

char* CCommand::getCommandName()
{
  return this->command_name;
}

class CHelpCommand : public CCommand
{
  private:
    CListIterator<CCommand> cmd_iter;

  public:
    virtual bool start_command(CStream* stream, char* argv[], int argc);
    virtual bool run_command(CStream* stream);
    virtual bool break_command(CStream* stream);

    virtual bool is_finished() {return !cmd_iter.get();};

    virtual void print_help(CStream* stream) {stream->printf("print this help message\n");};

    virtual ~CHelpCommand() {};
};

bool CHelpCommand::start_command(CStream* stream, char* argv[CCMD_MAX_ARGUMENTS_COUNT], int argc)
{
  UNUSED(argv);
  UNUSED(argc);

#if 0
  CArgParser  argP(argv, argc, F("asdfc:l:x:"));
  int         c;
  bool finish = false;

  count = 1;

  while (!finish)
  {
    c = argP.getSwitch();
    switch (c)
    {
      case 'c':
      {
        // getting opt argument
        stream->printf(F("Switch '%c' with argument %s\n"), c, argP.getSwitchArgument());
        break;
      }
      case 'x':
      {
        // getting opt argument
        stream->printf(F("Switch '%c' with argument %ld (sizeof(ld) = %d\n"), c, argP.getSwitchArgumentLong(), sizeof(long int));
        count = argP.getSwitchArgumentLong();
        break;
      }
      case 'l':
      {
        // getting opt argument
        stream->printf(F("Switch '%c' with argument %ld (sizeof(ld) = %d\n"), c, argP.getSwitchArgumentLong(), sizeof(long int));
        break;
      }
      default:
      {
        stream->printf(F("Switch '%c'\n"), c);
        break;
      }
      case CArgParser::CARG_END_OF_SWITCH:
      {
        stream->printf(F("End of switch\n"));
        finish = true;
        break;
      }
      case CArgParser::CARG_INVALID_ARG:
      {
        stream->printf(F("ERROR: Switch '%d'\n"), c);
        break;
      }
    }
  }
  stream->printf(F("argP.not_switch_idx == %d\n"), argP.not_switch_idx);
  for (int sw_idx = 0; sw_idx < argP.not_switch_idx; sw_idx++)
  {
    stream->printf(F("Additional param %s\n"), argv[sw_idx]);
  }
#endif
  cmd_iter.init(&cmdMgr.command_list);

  return true;
}
bool CHelpCommand::run_command(CStream* stream)
{
  if (stream->isOutBuffEmpty())
  {
    if (cmd_iter.get())
    {
      stream->printf(F(" %s : "), cmd_iter.get()->command_name);
      cmd_iter.get()->print_help(stream);
      cmd_iter.next();
    }
  }
  else
  {
    // wait for previous command delivery
  }
  return false;
}
bool CHelpCommand::break_command(CStream* stream)
{
  UNUSED(stream);
  return false;
}

CCommandsMgr  cmdMgr;
CHelpCommand  helpCmd;

void CCommandsMgr::Init()
{
  CRun::Init();
  command_list.init();
  current_command = NULL;
  current_stream = NULL;

  helpCmd.init((char*)"help");
}

void CCommandsMgr::registerCommand(CCommand* cmd_p)
{
  command_list.add_item(cmd_p);
}

int CCommandsMgr::parseSingleQuote(char* command, command_parse_error_t* error_code, char** arguments_pp)
{
  int idx = 1;
  int in_len = strlen(command);
  bool finish = false;
  do
  {
    switch (command[idx])
    {
      case '\'':
      {
        idx++;
        finish = true;
        break;
      }
      case '\0':
      {
        // end of the string
        // unexpected termination of the quote
        *error_code = PARSE_UNTERMINATED_SINGLE_QUOTE;
        finish = true;
        break;
      }
      default:
      {
        *(*arguments_pp) = command[idx];
        (*arguments_pp)++;
        idx++;
        break;
      }
    }
  } while ((idx <= in_len) && (!finish) && (*error_code == PARSE_OK));
  return idx;
}

int CCommandsMgr::parseEscape(char* command, command_parse_error_t* error_code, char** arguments_pp)
{
  // see http://www.tldp.org/LDP/abs/html/escapingsection.html
  int idx = 1;
  int in_len = strlen(command);
  bool finish = false;
  do
  {
    switch (command[idx])
    {
      case 'n':
      {
        *(*arguments_pp) = '\n';
        (*arguments_pp)++;
        idx++;
        finish = true;
        break;
      }
      case 'r':
      {
        *(*arguments_pp) = '\r';
        (*arguments_pp)++;
        idx++;
        finish = true;
        break;
      }
      case 't':
      {
        *(*arguments_pp) = '\t';
        (*arguments_pp)++;
        idx++;
        finish = true;
        break;
      }
      case 'v':
      {
        *(*arguments_pp) = '\v';
        (*arguments_pp)++;
        idx++;
        finish = true;
        break;
      }
      case 'b':
      {
        *(*arguments_pp) = '\b';
        (*arguments_pp)++;
        idx++;
        finish = true;
        break;
      }
      case 'a':
      {
        *(*arguments_pp) = '\a';
        (*arguments_pp)++;
        idx++;
        finish = true;
        break;
      }
      case '"':
      {
        *(*arguments_pp) = '"';
        (*arguments_pp)++;
        idx++;
        finish = true;
        break;
      }
      case '$':
      {
        *(*arguments_pp) = '$';
        (*arguments_pp)++;
        idx++;
        finish = true;
        break;
      }
      case '\\':
      {
        *(*arguments_pp) = '\\';
        (*arguments_pp)++;
        idx++;
        finish = true;
        break;
      }
      case '0':
      {
        // parse octal ASCII
        break;
      }
      case 'x':
      {
        // parse hex ASCII
        break;
      }
      case '\0':
      {
        // end of the string
        *error_code = PARSE_UNTERMINATED_ESCAPE_CHAR;
        finish = true;
        break;
      }
      default:
      {
        *error_code = PARSE_UNKNOWN_ESCAPE_CHAR;
        finish = true;
        break;
      }
    }
  } while ((idx <= in_len) && (!finish) && (*error_code == PARSE_OK));
  return idx;
}

int CCommandsMgr::parseDoubleQuote(char* command, command_parse_error_t* error_code, char** arguments_pp)
{
  int idx = 1;
  int in_len = strlen(command);
  bool finish = false;
  do
  {
    switch (command[idx])
    {
      case '"':
      {
        idx++;
        finish = true;
        break;
      }
      case '\\':
      {
        // parse escape character
        idx += parseEscape(&command[idx], error_code, arguments_pp);
        break;
      }
      case '\0':
      {
        // end of the string
        // unexpected termination of the quote
        *error_code = PARSE_UNTERMINATED_DOUBLE_QUOTE;
        finish = true;
        break;
      }
      default:
      {
        *(*arguments_pp) = command[idx];
        (*arguments_pp)++;
        idx++;
        break;
      }
    }
  } while ((idx <= in_len) && (!finish) && (*error_code == PARSE_OK));
  return idx;
}

int CCommandsMgr::parseCommand(char* command_p, command_parse_error_t* error_code_p, char* argv_p[], int* argc_p, char* arguments_p)
{
  int idx = 0;
  int in_len = strlen(command_p);
  bool finish = false;
  char* curr_arg_p;

  // first argument
  argv_p[*argc_p] = arguments_p;
  curr_arg_p = arguments_p;
  (*argc_p)++;

  do
  {
    switch (command_p[idx])
    {
      case '\'':
      {
        idx += parseSingleQuote(&command_p[idx], error_code_p, &arguments_p);
        break;
      }
      case '"':
      {
        idx += parseDoubleQuote(&command_p[idx], error_code_p, &arguments_p);
        break;
      }
      case '\\':
      {
        // parse escape character
        idx += parseEscape(&command_p[idx], error_code_p, &arguments_p);
        break;
      }
      case ' ':
      case '\t':
      {
        if (*argc_p < CCMD_MAX_ARGUMENTS_COUNT)
        {
          // argument terminated zero
          if (curr_arg_p == arguments_p)
          {
            // remove trailing spaces
            // skip space
          }
          else
          {
            // next argument
            *arguments_p = '\0';
            arguments_p++;

            argv_p[(*argc_p)] = arguments_p;
            curr_arg_p = arguments_p;
            (*argc_p)++;
          }
          idx++;
        }
        else
        {
          // to large number of arguments
          *error_code_p = PARSE_TOO_LARGE_ARGUMENTS_NUMBER;
          finish = true;
        }
        break;
      }
      case ';':
      case '\0':
      case '\n':
      {
        if (curr_arg_p == arguments_p)
        {
          // last argument - only spaces - remove it
          (*argc_p)--;
        }
        //command delimiter
        *arguments_p = '\0';
        arguments_p++;
        finish = true;
        break;
      }
      default:
      {
        *arguments_p = command_p[idx];
        arguments_p++;
        idx++;
        break;
      }
    }
  } while ((idx <= in_len) && (!finish) && (*error_code_p == PARSE_OK));

  return idx;
}

CCommand* CCommandsMgr::findCommand(char* command)
{
  CListIterator<CCommand> cmd_iter(&command_list);
  while(cmd_iter.get())
  {
//    TRACE_USER("Compare with '%s' == '%s'\n", cmd_iter.get()->command_name, command);
    if (strcmp(cmd_iter.get()->command_name, command) == 0)
    {
      return cmd_iter.get();
    }
    cmd_iter.next();
  }
  return NULL;
}

CStream* CCommandsMgr::getOutStream(void)
{
  return output_stream;
}

bool CCommandsMgr::exec_commands(char* command, CStream* stream)
{
  static char* argv[CCMD_MAX_ARGUMENTS_COUNT];      // static - not store on the stack
  static char  arguments[CCMD_MAX_COMMAND_LENGTH];  // static - not store on the stack

  int   idx = 0;
  int   in_len = strlen(command);
  int   argc;
  command_parse_error_t error_code = PARSE_OK;
  bool  finish = false;

  // check if we executing any command
  if (current_command != NULL)
  {
    // break current command
    current_command->break_command(current_stream);
    current_command = NULL;
    current_stream = NULL;
  }

  // output stream is the latest execution command stream
  output_stream = stream;

  // divide received command into commands
  do
  {
    memset(argv, 0, sizeof(argv));
    argc = 0;
    memset(arguments, 0, sizeof(arguments));

    idx += parseCommand(&command[idx], &error_code, argv, &argc, arguments);

    if ((argc > 0) &&
        (strlen(argv[0]) > 0) &&
        (error_code == PARSE_OK))
    {
      // execute command
      CCommand* cmd = findCommand(argv[0]);
      if (cmd != NULL)
      {
        // TODO add return value handling
        cmd->start_command(stream, argv, argc);
        stream->flushStream();

        if (!cmd->is_finished())
        {
          // command execution finished
          current_command = cmd;
          current_stream = stream;

          // break execution of the next commands - current command needs more time
          finish = true;
        }
      }
      else
      {
        // TODO command not found
        TRACE_ERROR("Command not found '%s'\n", argv[0]);
      }
    }
    idx++;
  } while ((idx <= in_len) &&
      (error_code == PARSE_OK) &&
      (finish == false));

  if (error_code != PARSE_OK)
  {
    TRACE_ERROR("Error %d with parse on position %d\n", error_code, idx);
    return false;
  }
  return true;
}

void CCommandsMgr::run()
{
  if (current_command != NULL)
  {
    if (!current_command->is_finished())
    {
      // command execution not finished
      current_command->run_command(current_stream);
      current_stream->flushStream();
    }
    if (current_command->is_finished())
    {
      current_stream->flushStream();
      // command execution finished
      current_command = NULL;
      current_stream = NULL;
    }
  }
}

bool CCommandsMgr::isIdle()
{
  if (current_command != NULL)
  {
    return current_command->is_idle();
  }
  return true;
}

