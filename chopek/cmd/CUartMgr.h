/*
 * CUartMgr.h
 *
 *  Created on: 26-11-2013
 *      Author: Przemek Kieszkowski
 */

#ifndef CUARTMGR_H_
#define CUARTMGR_H_

#include "CRun.h"
#include "CSettingsMgr.h"
#include "sysTimer.h"
#include "CStream.h"
#include "halUart.h"

#define CUART_PRINTF_BUFF_SIZE  HAL_UART_TX_FIFO_SIZE
#define CUART_CMD_MAX_CMD_LEN   HAL_UART_TX_FIFO_SIZE

class CUartMgr : public CRun, public CStream
{
  private:
    CSetting<uint8_t,  SETTING_UINT8>uartSleepEnable;
    CSetting<uint32_t, SETTING_UINT32>uartSleep;
    CSetting<uint32_t, SETTING_UINT32>uartCheckForActive;
    CSetting<uint32_t, SETTING_UINT32>uartActive;

    volatile bool bytes_received;

    enum uart_state_t {
      UART_SLEEP,
      UART_CHECK_FOR_ACTIVE,
      UART_ACTIVE,
      UART_ALWAYS_ACTIVE,
    } ;

    uart_state_t  state;

    SYS_Timer_t uart_timer;

    void startSleepState();
    void startCheckForActiveState();
    void startActiveState();
    void startAlwaysActiveState();

    char    print_buff[CUART_PRINTF_BUFF_SIZE];
    // buffer where out stream stores data before send it in one shot
    char    uart_out_buff[CUART_CMD_MAX_CMD_LEN];
    uint8_t uart_out_buff_len;
    
    char    uart_cmd[CUART_CMD_MAX_CMD_LEN];
    uint8_t uart_cmd_len;

    void printPrompt();
    void printErrorMessage();

  public:
    void Init();
    virtual void run();
    virtual bool isIdle();

    virtual char* getPrintfBuffer();
    virtual int   getPrintfBufferSize();
    virtual void  write(char c);

    virtual uint16_t getRemainingOutBuffSize();
    virtual bool isOutBuffEmpty();
    virtual bool flushStream();


    void uartBytesReceived(uint16_t bytes);

    // for eclipse happiness
    virtual ~CUartMgr() {};

    friend void uartTimerHandler(SYS_Timer_t *timer);
    friend void HAL_Uart0BytesReceived(uint16_t bytes);
};

extern CUartMgr uartMgr;

#endif /* CUARTMGR_H_ */
