/*
 * net_util.h
 *
 * Created: 2015-01-30 12:35:58
 *  Author: przemek
 */ 


#ifndef NET_UTIL_H_
#define NET_UTIL_H_

#include "tal.h"

#ifdef __cplusplus
extern "C" {
#endif

void print_frame_info_t(frame_info_t* fi_p);

#ifdef __cplusplus
}
#endif

#endif /* NET_UTIL_H_ */