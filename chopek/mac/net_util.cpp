/*
 * net_util.cpp
 *
 * Created: 2015-01-30 12:36:50
 *  Author: przemek
 */ 

#include "net_util.h"
#include "trace.h"

void print_frame_info_t(frame_info_t* fi_p)
{
#if defined(TRACE_LEVEL)
  TRACE_USER("Frame msg_type %d\n", (int)fi_p->msg_type);

#if 0  
  frame_msgtype_t
  typedef enum {
	/* MAC Command Frames (table 67) */
	/* Command Frame Identifier for Association Request */
	ASSOCIATIONREQUEST          = (0x01),
	/* Command Frame Identifier for Association Response */
	ASSOCIATIONRESPONSE,
	/* Command Frame Identifier for Disassociation Notification */
	DISASSOCIATIONNOTIFICATION,
	/* Command Frame Identifier for Data Request */
	DATAREQUEST,
	/* Command Frame Identifier for PANID Conflict Notification */
	PANIDCONFLICTNOTIFICAION,
	/* Command Frame Identifier for Orphan Notification */
	ORPHANNOTIFICATION,
	/* Command Frame Identifier for Beacon Request */
	BEACONREQUEST,
	/* Command Frame Identifier for Coordinator Realignment */
	COORDINATORREALIGNMENT,

	GTSREQUEST, 9

	/*
	 * These are not MAC command frames but listed here as they are needed
	 * in the msgtype field
	 */
	/* Message is a directed orphan realignment command */
	ORPHANREALIGNMENT,
	/* Message is a beacon frame (in response to a beacon request cmd) */
	BEACON_MESSAGE,
	/* Message type field value for implicite poll without request */
	DATAREQUEST_IMPL_POLL,
	/* Message type field value for Null frame */
	NULL_FRAME,
	/* Message type field value for MCPS message */
	MCPS_MESSAGE
#ifdef ENABLE_RTB
	,
	/* RTB Message Types */
	/* Message type field value for RTB Range Request frame */
	RTB_CMD_RANGE_REQ,
	/* Message type field value for RTB Range Accept frame */
	RTB_CMD_RANGE_ACPT,
	/* Message type field value for RTB PMU Time Sync Request frame */
	RTB_CMD_PMU_TIME_SYNC_REQ,
	/* Message type field value for RTB PMU Start frame */
	RTB_CMD_PMU_PMU_START,
	/* Message type field value for RTB Result Request frame */
	RTB_CMD_RESULT_REQ,
	/* Message type field value for RTB Result Confirm frame */
	RTB_CMD_RESULT_CONF,
	/* Message type field value for RTB Remote Range Request frame */
	RTB_CMD_REMOTE_RANGE_REQ,
	/* Message type field value for RTB Remote Range Confirm frame */
	RTB_CMD_REMOTE_RANGE_CONF,
	/* Message type field value for RTB Remote Range Band Results frame */
	RTB_CMD_REMOTE_RANGE_BAND_RESULTS
#endif  /* ENABLE_RTB */
} SHORTENUM frame_msgtype_t;
#endif
  
  switch (fi_p->msg_type)
  {
	  /* Command Frame Identifier for Association Request */
    case  ASSOCIATIONREQUEST:
    
	  /* Command Frame Identifier for Association Response */
	  case  ASSOCIATIONRESPONSE:
	  /* Command Frame Identifier for Disassociation Notification */
	  case  DISASSOCIATIONNOTIFICATION:
	  /* Command Frame Identifier for Data Request */
	  case  DATAREQUEST:
	  /* Command Frame Identifier for PANID Conflict Notification */
	  case  PANIDCONFLICTNOTIFICAION:
	  /* Command Frame Identifier for Orphan Notification */
	  case  ORPHANNOTIFICATION:
	  /* Command Frame Identifier for Beacon Request */
	  case  BEACONREQUEST:
	  /* Command Frame Identifier for Coordinator Realignment */
	  case  COORDINATORREALIGNMENT:
    {
      // currently nothing to do
      break;
    }
    case   GTSREQUEST:
    {
      break;
    }
	  /*
	   * These are not MAC command frames but listed here as they are needed
	   * in the msgtype field
	   */
	  /* Message is a directed orphan realignment command */
	  case  ORPHANREALIGNMENT:
	  /* Message is a beacon frame (in response to a beacon request cmd) */
	  case  BEACON_MESSAGE:
	  /* Message type field value for implicite poll without request */
	  case  DATAREQUEST_IMPL_POLL:
	  /* Message type field value for Null frame */
	  case  NULL_FRAME:
	  /* Message type field value for MCPS message */
	  case  MCPS_MESSAGE:
#ifdef ENABLE_RTB
	  /* RTB Message Types */
	  /* Message type field value for RTB Range Request frame */
	  case  RTB_CMD_RANGE_REQ:
	  /* Message type field value for RTB Range Accept frame */
	  case  RTB_CMD_RANGE_ACPT:
	  /* Message type field value for RTB PMU Time Sync Request frame */
	  case  RTB_CMD_PMU_TIME_SYNC_REQ:
	  /* Message type field value for RTB PMU Start frame */
	  case  RTB_CMD_PMU_PMU_START:
	  /* Message type field value for RTB Result Request frame */
	  case  RTB_CMD_RESULT_REQ:
	  /* Message type field value for RTB Result Confirm frame */
	  case  RTB_CMD_RESULT_CONF:
	  /* Message type field value for RTB Remote Range Request frame */
	  case  RTB_CMD_REMOTE_RANGE_REQ:
	  /* Message type field value for RTB Remote Range Confirm frame */
	  case  RTB_CMD_REMOTE_RANGE_CONF:
	  /* Message type field value for RTB Remote Range Band Results frame */
	  case  RTB_CMD_REMOTE_RANGE_BAND_RESULTS:
#endif  /* ENABLE_RTB */
    {
      // currently nothing to do
      break;
    }
  }
  
#else
  UNUSED(fi_p);
#endif
}
