/*
 * CLed.h
 *
 *  Created on: 02-12-2013
 *      Author: Przemek Kieszkowski
 */

#ifndef CLED_H_
#define CLED_H_

#include "CPin.h"

class CLed
{
  private:
    CPin* pin;
    bool  off_state;
  public:
    void Init(CPin* pin_p, bool on_state);

    void Set(bool on);
    void Toggle();
};

#endif /* CLED_H_ */
