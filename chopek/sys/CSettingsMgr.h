/*
 * CSettingsMgr.h
 *
 *  Created on: 20-11-2013
 *      Author: Przemek Kieszkowski
 */

#ifndef CSETTINGSMGR_H_
#define CSETTINGSMGR_H_

#include "sysTypes.h"
#include <stdio.h>


#define SYS_SETTING_MAX_NAME_LENGTH   16
#define SYS_SETTING_MAX_VALUE_LENGTH  250
#define SYS_SETTING_INVALID           0

typedef enum
{
  SETTING_EMPTY   = 0,
  SETTING_STRING,
  SETTING_UINT8,
  SETTING_UINT16,
  SETTING_UINT32,
  SETTING_UINT8_ARRAY,
  SETTING_UINT16_ARRAY,
  SETTING_UINT32_ARRAY,

  SETTING_LAST,
}setting_type_t;

typedef struct PACK setting_value_t
{
  uint8_t   setting_type;   //sys_settings_types_t
  uint8_t   name_len;
  uint8_t   value_len;
}setting_value_t;

typedef uint16_t  sett_addr_t;

class CSettingBase
{
  private:
  public:
    sett_addr_t       sett_addr;
    setting_value_t   sett_struct;
    char*             sett_name;
    setting_type_t    type;

    bool RegisterSetting(uint8_t maxLen, uint8_t* initialValue, uint8_t initialLen);
    bool FindSetting();
    sett_addr_t getSettingStructSize();

    uint16_t getSettingSize();

    void readSetting(uint8_t* dst, uint8_t len);
    void writeSetting(uint8_t* src, uint8_t len);

};

class CSettingsMgr
{
  private:
    void print_trace(char* trace);
  public:
    void Init();
    void FactorySettings(void);

    // for eclipse happiness
    virtual ~CSettingsMgr() {};

    template<class T, setting_type_t TYPE> friend class CSetting;
    template<class T, setting_type_t TYPE, int ARR_SIZE> friend class CSettingArray;
};

extern CSettingsMgr settingsMgr;

template <class T, setting_type_t TYPE>
class CSetting : public CSettingBase
{
  private:
    T                 value;
  public:
    CSetting()
    {
      value     = 0;
      type      = TYPE;
      sett_addr = 0;
      sett_name = (char*)"";
    }
    CSetting(char* settingName, T defaultValue)
    {
      type = TYPE;
      Init(settingName, defaultValue);
    }

    void Init(char* settingName, T initialValue)
    {
      sett_name = settingName;
      type = TYPE;
      RegisterSetting(getSettingSize(), (uint8_t*)&initialValue, getSettingSize());
      readSetting((uint8_t*)&value, (uint8_t)getSettingSize());
    }

    uint16_t getSettingSize()
    {
      return sizeof(T);
    }

    uint32_t  getUI()
    {
      return (uint32_t)value;
    }
    T get()
    {
      return value;
    };
    void set(T new_value)
    {
      value = new_value;
      writeSetting((uint8_t*)&value, (uint8_t)getSettingSize());
    };

};

template <class T, setting_type_t TYPE, int ARR_SIZE>
class CSettingArray : public CSettingBase
{
  private:
    T                 value[ARR_SIZE];
  public:
    CSettingArray()
    {
      type      = TYPE;
      sett_addr = 0;
      sett_name = (char*)"";
    }
    CSettingArray(char* settingName, T defaultValue)
    {
      type = TYPE;
      Init(settingName, defaultValue);
    }

    void Init(char* settingName, T* initialValue)
    {
      sett_name = settingName;
      type = TYPE;
      RegisterSetting(getSettingSize(), (uint8_t*)initialValue, getSettingSize());
      readSetting((uint8_t*)value, (uint8_t)getSettingSize());
    }

    uint16_t getSettingSize()
    {
      return sizeof(T) * ARR_SIZE;
    }

    T* get()
    {
      return value;
    };
    T getItem(int idx)
    {
      return value[idx];
    }

    void set(T* new_value)
    {
      memcpy(value, new_value, getSettingSize());
      writeSetting((uint8_t*)value, getSettingSize());
    };
    void setItem(T new_value, int idx)
    {
      value[idx] = new_value;
      writeSetting((uint8_t*)value, getSettingSize());
    }

};
#endif /* CSETTINGSMGR_H_ */
