/*
 * sysSettings.h
 *
 *  Created on: 30-07-2013
 *      Author: Przemek Kieszkowski
 */

#ifndef SYSSETTINGS_H_
#define SYSSETTINGS_H_

#include <stdint.h>
#include <avr/pgmspace.h>
#include "sysTypes.h"

#ifdef __cplusplus
extern "C"
{
#endif
	
#define SYS_SETTING_MAX_NAME_LENGTH   16
#define SYS_SETTING_MAX_VALUE_LENGTH	250
#define SYS_SETTING_INVALID						0

typedef enum
{
  SYS_SETTING_EMPTY   = 0,
  SYS_SETTING_STRING,
  SYS_SETTING_UINT8,
  SYS_SETTING_UINT16,
  SYS_SETTING_UINT32,
  SYS_SETTING_UINT8_ARRAY,
  SYS_SETTING_UINT16_ARRAY,
  SYS_SETTING_UINT32_ARRAY,

  SYS_SETTING_LAST,
}sys_setting_type_t;

typedef struct PACK sys_setting_value_t
{
  uint8_t   setting_type;   //sys_settings_types_t
  uint8_t   name_len;
  uint8_t   value_len;
}sys_setting_value_t;

typedef uint16_t	sys_sett_addr_t;

typedef struct PACK setting_idx_t
{
	sys_sett_addr_t				sett_addr;
	sys_setting_value_t		sett_struct;
}setting_idx_t;

void      			SYS_SettingsInit(void);
setting_idx_t*	SYS_FindSetting(const char* settingName, setting_idx_t* sett_idx_p);

setting_idx_t*	SYS_SettingRegisterString(setting_idx_t* idx_p, const char* settingName, uint8_t maxLen, char* defaultValue);
uint8_t       	SYS_SettingSetString(setting_idx_t* idx_p, char* value);
uint8_t       	SYS_SettingGetString(setting_idx_t* idx_p, char* buff, uint8_t buff_size);

setting_idx_t*	SYS_SettingRegisterUInt8(setting_idx_t* idx_p, const char* settingName, uint8_t defaultValue);
uint8_t       	SYS_SettingSetUInt8(setting_idx_t* idx_p, uint8_t value);
uint8_t       	SYS_SettingGetUInt8(setting_idx_t* idx_p, uint8_t* value_p);

setting_idx_t*	SYS_SettingRegisterUInt16(setting_idx_t* idx_p, const char* settingName, uint16_t defaultValue);
uint8_t       	SYS_SettingSetUInt16(setting_idx_t* idx_p, uint16_t value);
uint8_t       	SYS_SettingGetUInt16(setting_idx_t* idx_p, uint16_t* value_p);

setting_idx_t*	SYS_SettingRegisterUInt32(setting_idx_t* idx_p, const char* settingName, uint32_t defaultValue);
uint8_t       	SYS_SettingSetUInt32(setting_idx_t* idx_p, uint32_t value);
uint8_t       	SYS_SettingGetUInt32(setting_idx_t* idx_p, uint32_t* value_p);

uint8_t 				SYS_SettingGetValue(setting_idx_t* idx_p, uint8_t* buff, uint8_t buff_size);
uint8_t 				SYS_SettingSetValue(setting_idx_t* idx_p, uint8_t* buff, uint8_t buff_size);
uint8_t 				SYS_SettingGetName(setting_idx_t* idx_p, char* buff, uint8_t buff_size);

/**
 * function to list all available settings
 * For the first call put SYS_SETTING_INVALID as a prev_idx->sett_addr
 * It returns NULL if no more settings are available
 */
setting_idx_t* SYS_SettingNext(setting_idx_t* prev_idx_p);

#ifdef __cplusplus
}
#endif

#endif /* SYSSETTINGS_H_ */
