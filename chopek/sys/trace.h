/* ----------------------------------------------------------------------------
 *         ATMEL Microcontroller Software Support
 * ----------------------------------------------------------------------------
 * Copyright (c) 2008, Atmel Corporation
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * Atmel's name may not be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ----------------------------------------------------------------------------
 */

//------------------------------------------------------------------------------
/// \unit
///
/// !Purpose
///
/// Standard output methods for reporting debug information, warnings and
/// errors, which can be easily be turned on/off.
///
/// !Usage
/// -# Initialize the DBGU using TRACE_CONFIGURE() if you intend to eventually
///    disable ALL traces; otherwise use DBGU_Configure().
/// -# Uses the TRACE_DEBUG(), TRACE_INFO(), TRACE_WARNING(), TRACE_ERROR()
///    TRACE_FATAL() macros to output traces throughout the program.
/// -# Each type of trace has a level : Debug 5, Info 4, Warning 3, Error 2
///    and Fatal 1. Disable a group of traces by changing the value of
///    TRACE_LEVEL during compilation; traces with a level bigger than TRACE_LEVEL
///    are not generated. To generate no trace, use the reserved value 0.
/// -# Trace disabling can be static or dynamic. If dynamic disabling is selected
///    the trace level can be modified in runtime. If static disabling is selected
///    the disabled traces are not compiled.
///
/// !Trace level description
/// -# TRACE_DEBUG (5): Traces whose only purpose is for debugging the program,
///    and which do not produce meaningful information otherwise.
/// -# TRACE_INFO (4): Informational trace about the program execution. Should
///    enable the user to see the execution flow.
/// -# TRACE_WARNING (3): Indicates that a minor error has happened. In most case
///    it can be discarded safely; it may even be expected.
/// -# TRACE_ERROR (2): Indicates an error which may not stop the program execution,
///    but which indicates there is a problem with the code.
/// -# TRACE_FATAL (1): Indicates a major error which prevents the program from going
///    any further.

//------------------------------------------------------------------------------
#ifdef TRACE_H
#error TRACE_H Already included !!!
#endif

#ifndef TRACE_H
#define TRACE_H

#include <stdio.h>
#include "CFlashString.h"

#ifdef __cplusplus
	extern "C" {
#endif

//------------------------------------------------------------------------------
//         Headers
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//         Global Definitions
//------------------------------------------------------------------------------

/// Softpack Version
#define SOFTPACK_VERSION "1.5"

#define TRACE_LEVEL_DEBUG      6
#define TRACE_LEVEL_INFO       5
#define TRACE_LEVEL_USER       4
#define TRACE_LEVEL_WARNING    3
#define TRACE_LEVEL_ERROR      2
#define TRACE_LEVEL_FATAL      1
#define TRACE_LEVEL_NO_TRACE   0

// By default, all traces are output except the debug one.
#if !defined(TRACE_LEVEL)
//#define TRACE_LEVEL TRACE_LEVEL_DEBUG //TRACE_LEVEL_INFO
#define TRACE_LEVEL TRACE_LEVEL_INFO
#endif

// By default, trace level is static (not dynamic)
#if !defined(DYN_TRACES)
#define DYN_TRACES 0
#endif

#if defined(NOTRACE)
#error "Error: NOTRACE has to be not defined !"
#endif

#undef NOTRACE
#if (TRACE_LEVEL == TRACE_LEVEL_NO_TRACE)
#define NOTRACE
#endif


//------------------------------------------------------------------------------
//         Global Macros
//------------------------------------------------------------------------------

#if 0
//------------------------------------------------------------------------------
/// Initializes the DBGU
/// \param mode  DBGU mode.
/// \param baudrate  DBGU baudrate.
/// \param mck  Master clock frequency.
//------------------------------------------------------------------------------
#define TRACE_CONFIGURE(mode, baudrate, mck) { \
    const Pin pinsDbgu[] = {PINS_DBGU}; \
    PIO_Configure(pinsDbgu, PIO_LISTSIZE(pinsDbgu)); \
    DBGU_Configure(mode, baudrate, mck); \
    }
#endif

//------------------------------------------------------------------------------
/// Initializes the DBGU for ISP project
/// \param mode  DBGU mode.
/// \param baudrate  DBGU baudrate.
/// \param mck  Master clock frequency.
//------------------------------------------------------------------------------
#if (TRACE_LEVEL==0) && (DYNTRACE==0)
#define TRACE_CONFIGURE_ISP(mode, baudrate, mck) {}
#else
#define TRACE_CONFIGURE_ISP(mode, baudrate, mck) { \
    const Pin pinsDbgu[] = {PINS_DBGU}; \
    PIO_Configure(pinsDbgu, PIO_LISTSIZE(pinsDbgu)); \
    DBGU_Configure(mode, baudrate, mck); \
    }
#endif

#if 0
extern xSemaphoreHandle xprintSem;

#define TRACE_LOCK     {xSemaphoreTake(xprintSem, 1000 / portTICK_RATE_MS);}
#define TRACE_UNLOCK   {xSemaphoreGive(xprintSem);}
#else
#define TRACE_LOCK
#define TRACE_UNLOCK
#endif

//------------------------------------------------------------------------------
/// Outputs a formatted string using <printf> if the log level is high
/// enough. Can be disabled by defining TRACE_LEVEL=0 during compilation.
/// \param format  Formatted string to output.
/// \param ...  Additional parameters depending on formatted string.
//------------------------------------------------------------------------------
#if defined(NOTRACE)

// Empty macro
#define TRACE_DEBUG(...)      { }
#define TRACE_INFO(...)       { }
#define TRACE_USER(...)       { }
#define TRACE_WARNING(...)    { }
#define TRACE_ERROR(...)      { }
#define TRACE_FATAL(...)      { while(1); }

#define TRACE_DEBUG_WP(...)   { }
#define TRACE_INFO_WP(...)    { }
#define TRACE_USER_WP(...)    { }
#define TRACE_WARNING_WP(...) { }
#define TRACE_ERROR_WP(...)   { }
#define TRACE_FATAL_WP(...)   { while(1); }

#elif (DYN_TRACES == 1)

// Trace output depends on traceLevel value
#define TRACE_DEBUG(...)      { if (traceLevel >= TRACE_LEVEL_DEBUG)   { printf_P("-D- " __VA_ARGS__); } }
#define TRACE_INFO(...)       { if (traceLevel >= TRACE_LEVEL_INFO)    { printf_P("-I- " __VA_ARGS__); } }
#define TRACE_USER(...)       { if (traceLevel >= TRACE_LEVEL_USER)    { printf_P("-U- " __VA_ARGS__); } }
#define TRACE_WARNING(...)    { if (traceLevel >= TRACE_LEVEL_WARNING) { printf_P("-W- " __VA_ARGS__); } }
#define TRACE_ERROR(...)      { if (traceLevel >= TRACE_LEVEL_ERROR)   { printf_P("-E- " __VA_ARGS__); } }
#define TRACE_FATAL(...)      { if (traceLevel >= TRACE_LEVEL_FATAL)   { printf_P("-F- " __VA_ARGS__); while(1); } }

#else

extern unsigned char uart0Initialized;

// Trace compilation depends on TRACE_LEVEL value
#if (TRACE_LEVEL >= TRACE_LEVEL_DEBUG)
#define TRACE_DEBUG(format, ...)		{if (uart0Initialized) {TRACE_LOCK printf_P((const char*)F(__FILE__ ":%4d " format), __LINE__, ## __VA_ARGS__); TRACE_UNLOCK}}
#else
#define TRACE_DEBUG(...)      { }
#endif

#if (TRACE_LEVEL >= TRACE_LEVEL_INFO)
#define TRACE_INFO(format, ...)		{if (uart0Initialized) {TRACE_LOCK printf_P((const char*)F(__FILE__ ":%4d " format), __LINE__, ## __VA_ARGS__); TRACE_UNLOCK}}
#else
#define TRACE_INFO(...)       { }
#endif

#if (TRACE_LEVEL >= TRACE_LEVEL_USER)
#define TRACE_USER(format, ...)		{if (uart0Initialized) {TRACE_LOCK printf_P((const char*)F(__FILE__ ":%4d " format), __LINE__, ## __VA_ARGS__); TRACE_UNLOCK}}
#else
#define TRACE_USER(...)       { }
#endif

#if (TRACE_LEVEL >= TRACE_LEVEL_WARNING)
#define TRACE_WARNING(format, ...)		{if (uart0Initialized) {TRACE_LOCK printf_P((const char*)F(__FILE__ ":%4d -W- " format), __LINE__, ## __VA_ARGS__); TRACE_UNLOCK}}
#else
#define TRACE_WARNING(...)    { }
#endif

#if (TRACE_LEVEL >= TRACE_LEVEL_ERROR)
#define TRACE_ERROR(format, ...)		{if (uart0Initialized) {TRACE_LOCK printf_P((const char*)F(__FILE__ ":%4d -E- " format), __LINE__, ## __VA_ARGS__); TRACE_UNLOCK}}
#else
#define TRACE_ERROR(...)      { }
#endif

#if (TRACE_LEVEL >= TRACE_LEVEL_FATAL)
#define TRACE_FATAL(format, ...)		{{{TRACE_LOCK printf_P((const char*)F(__FILE__ ":%4d ASSERT " format), __LINE__, ## __VA_ARGS__); TRACE_UNLOCK}} while(1); }
#else
#define TRACE_FATAL(...)      { while(1); }
#endif

#define Assert(expr) \
	{\
		if (!(expr)) { TRACE_ERROR(#expr "\n") ;}\
	}

#endif


//------------------------------------------------------------------------------
//         Exported variables
//------------------------------------------------------------------------------
// Depending on DYN_TRACES, traceLevel is a modifiable runtime variable
// or a define
#if !defined(NOTRACE) && (DYN_TRACES == 1)
    extern unsigned int traceLevel;
#endif

#ifdef __cplusplus
}
#endif

#endif //#ifndef TRACE_H

