/**
 * \file sysTimer.c
 *
 * \brief System timer implementation
 *
 * Copyright (C) 2012-2014, Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 * Modification and other use of this code is subject to Atmel's Limited
 * License Agreement (license.txt).
 *
 * $Id: sysTimer.c 9267 2014-03-18 21:46:19Z ataradov $
 *
 */

/*- Includes ---------------------------------------------------------------*/
#include <stdlib.h>
#include "hal.h"
#include "halRTC.h"
#include "sysTimer.h"

#include "trace.h"

/*- Prototypes -------------------------------------------------------------*/
static void placeTimer(SYS_Timer_t *timer);

/*- Variables --------------------------------------------------------------*/
static SYS_Timer_t *timers;

/*- Implementations --------------------------------------------------------*/

/*************************************************************************//**
*****************************************************************************/
void SYS_TimerInit(void)
{
  timers = NULL;
}

/*************************************************************************//**
*****************************************************************************/
void SYS_TimerStart(SYS_Timer_t *timer)
{
  if (!SYS_TimerStarted(timer))
    placeTimer(timer);
}

void SYS_TimerStart(SYS_Timer_t *timer, CTimerFired* class_handler)
{
  timer->handler = NULL;
  timer->class_handler = class_handler;

  if (!SYS_TimerStarted(timer))
    placeTimer(timer);
}

/*************************************************************************//**
*****************************************************************************/
void SYS_TimerStop(SYS_Timer_t *timer)
{
  SYS_Timer_t *prev = NULL;

  for (SYS_Timer_t *t = timers; t; t = t->next)
  {
    if (t == timer)
    {
      if (prev)
        prev->next = t->next;
      else
        timers = t->next;

      break;
    }
    prev = t;
  }
}

/*************************************************************************//**
*****************************************************************************/
bool SYS_TimerStarted(SYS_Timer_t *timer)
{
  for (SYS_Timer_t *t = timers; t; t = t->next)
    if (t == timer)
      return true;
  return false;
}

void dump_timers(void)
{
  static SYS_Timer_t *timers_loc;

  timers_loc = timers;

  TRACE_USER("Current tick =0x%08lx\n", HAL_RTCGetTicks());
  while (timers_loc)
  {
    TRACE_USER("To fire 0x%08lx\n", timers_loc->fire_tick);
    timers_loc = timers_loc->next;
  }
}
/*************************************************************************//**
*****************************************************************************/
void SYS_TimerTaskHandler(void)
{
  static rtc_ticks_t last_tick_count = 0;
  rtc_ticks_t tick_count = HAL_RTCGetTicks();

  if (last_tick_count == tick_count)
  {
    return;
  }
  last_tick_count = tick_count;

//  while (timers && (timers->fire_tick <= tick_count))
  // -1 : rtc_clk < right
  //  0 : rtc_clk = right
  //  1 : rtc_clk > right
  while (timers && (HAL_RTCTickCmp(tick_count, timers->fire_tick) > -1))
  {
    SYS_Timer_t *timer = timers;

//    TRACE_USER("To fire 0x%08lx, 0x%08lx\n", timers->fire_tick, tick_count);

    timers = timers->next;
    if (SYS_TIMER_PERIODIC_MODE == timer->mode)
    {
      placeTimer(timer);
    }
    if (timer->handler != NULL)
    {
      timer->handler(timer);
    }
    else if (timer->class_handler != NULL)
    {
      timer->class_handler->timerFired(timer);
    }
  }
}

/*************************************************************************//**
*****************************************************************************/
uint32_t SYS_TimerGetNextFire(void)
{
  if (timers)
  {
    TRACE_USER("NF %d\n", (int)timers->fire_tick);
    return timers->fire_tick;
  }
  else
  {
    return 0;
  }
}

// -1 : left < right
//  0 : left = right
//  1 : left > right
//int8_t HAL_RTCTickCmp(const rtc_ticks_t& left, const rtc_ticks_t& right)

/*************************************************************************//**
*****************************************************************************/
static void placeTimer(SYS_Timer_t *timer)
{
  timer->fire_tick = HAL_RTCGetTicks() + HAL_RTCGetHWTicks_ms(timer->interval);
  if (timers)
  {
    SYS_Timer_t *prev = NULL;

    for (SYS_Timer_t *t = timers; t; t = t->next)
    {
//      TRACE_USER("place new 0x%08lx at 0x%08lx\n", timer->fire_tick, t->fire_tick);
        //  0 : rtc_clk = right
        //  1 : rtc_clk > right
      if  (HAL_RTCTickCmp(t->fire_tick, timer->fire_tick) == 1)
      {
         break;
      }
      else
      {
      }

      prev = t;
    }

    if (prev)
    {
      timer->next = prev->next;
      prev->next = timer;
    }
    else
    {
      timer->next = timers;
      timers = timer;
    }
  }
  else
  {
    timer->next = NULL;
    timers = timer;
  }
//  TRACE_USER("Place new timer: 0x%08lx\n", timer->fire_tick);
}
