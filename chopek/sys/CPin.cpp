/*
 * CPin.cpp
 *
 *  Created on: 01-12-2013
 *      Author: Przemek Kieszkowski
 */

#include "CPin.h"
#include "hal.h"
#include "halGpio.h"


//void CPin::CPin(pin_port_t port, uint8_t pin_num, pin_direction_t direction)
//{
//  Init(port, pin_num, direction);
//}

void CPin::Init(pin_port_t port, uint8_t pin_num, pin_direction_t direction)
{
  this->port = port;
  this->pin  = 1 << pin_num;
  this->direction = direction;

  switch (port)
  {
    case PIN_PORTA: portaddr = &PORTA; ddr_addr = &DDRA; pin_addr = &PINA; break;
    case PIN_PORTB: portaddr = &PORTB; ddr_addr = &DDRB; pin_addr = &PINB; break;
    case PIN_PORTC: portaddr = &PORTC; ddr_addr = &DDRC; pin_addr = &PINC; break;
    case PIN_PORTD: portaddr = &PORTD; ddr_addr = &DDRD; pin_addr = &PIND; break;
    case PIN_PORTE: portaddr = &PORTE; ddr_addr = &DDRE; pin_addr = &PINE; break;
    case PIN_PORTF: portaddr = &PORTF; ddr_addr = &DDRF; pin_addr = &PINF; break;
  }

  if (direction == PIN_IN)
  {
    In();
  }
  else
  {
    Out();
  }
}

void CPin::Set()
{
  *portaddr |= pin;
}
void CPin::Clr()
{
  *portaddr &= ~pin;
}

void CPin::Toggle()
{
  if (*portaddr & pin)
  {
    Clr();
  }
  else
  {
    Set();
  }
}

void CPin::Set(bool state)
{
  if (state)
  {
    Set();
  }
  else
  {
    Clr();
  }
}

void CPin::In()
{
  *ddr_addr &= ~pin;
  *portaddr &= ~pin; // no pullup
}

void CPin::Out()
{
  *ddr_addr |= pin;
}

void CPin::Pullup()
{
  *portaddr |= pin;
}

bool CPin::Read()
{
  return (*pin_addr & pin) != 0;
}

bool CPin::State()
{
  return (*ddr_addr & pin) != 0;
}


