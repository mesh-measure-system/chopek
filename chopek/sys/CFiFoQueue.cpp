/*
 * CFiFoQueue.cpp
 *
 *  Created on: 15-11-2013
 *      Author: Przemek Kieszkowski
 */

#include <stddef.h>

#include "CFiFoQueue.h"

#include "trace.h"


void CFiFoQueue::Init()
{
  firstItem_p = NULL;
}

bool CFiFoQueue::isEmpty()
{
  return firstItem_p == NULL;
}

bool CFiFoQueue::Push(CFiFoItem* item_p)
{
  // place to store new event
  CFiFoItem** cevt_pp = &firstItem_p;
  CFiFoItem*  cevt_p  =  firstItem_p;
  bool     duplicated = false;

  while(cevt_p != NULL)
  {
    // check for duplicated events
    if (cevt_p == item_p)
    {
      // duplicated found - rejecting
//      TRACE_ERROR("Found duplicated event\n");
      duplicated = true;
      break;
    }
    cevt_pp = &cevt_p->next_item_p;
    cevt_p  =  cevt_p->next_item_p;
  }

  if (!duplicated)
  {
    // added at the end of queue
    *cevt_pp = item_p;
    item_p->next_item_p = NULL;
  }

  return !duplicated;
}

void CFiFoQueue::Remove(CFiFoItem* evt_p)
{
  // place to remove event
  CFiFoItem** cevt_pp = &firstItem_p;
  CFiFoItem*  cevt_p  =  firstItem_p;

  while (cevt_p != NULL)
  {
    if (cevt_p == evt_p)
    {
      // found CFiFoItem to remove
      *cevt_pp = cevt_p->next_item_p;
      cevt_p->next_item_p = NULL;
      break;
    }
    cevt_pp = &cevt_p->next_item_p;
    cevt_p  =  cevt_p->next_item_p;
  }
}

CFiFoItem* CFiFoQueue::Pop()
{
  CFiFoItem* item_p = NULL;
  if (firstItem_p != NULL)
  {
    item_p = firstItem_p;
    Remove(item_p);
  }
  return item_p;
}

