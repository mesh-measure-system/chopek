/*
 * CFiFoQueue.h
 *
 *  Created on: 15-11-2013
 *      Author: Przemek Kieszkowski
 */

#ifndef CFIFOQUEUE_H_
#define CFIFOQUEUE_H_


class CFiFoItem
{
  private:
    CFiFoItem* next_item_p;
  public:
    friend class CFiFoQueue;
};

class CFiFoQueue
{
  private:
    CFiFoItem* firstItem_p;
  public:
    void Init();
    bool isEmpty();
    bool Push(CFiFoItem* item_p);
    void Remove(CFiFoItem* evt_p);
    CFiFoItem* Pop();

    virtual ~CFiFoQueue() {};
};

#endif /* CFIFOQUEUE_H_ */
