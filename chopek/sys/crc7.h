
#ifndef CRC7_H
#define CRC7_H

#include "sysSettings.h"

#ifdef __cplusplus
extern "C"
{
#endif

uint8_t crc7(uint8_t crc, const uint8_t *buffer, uint16_t len);

#ifdef __cplusplus
}
#endif

#endif

