/*
 * CPin.h
 *
 *  Created on: 01-12-2013
 *      Author: Przemek Kieszkowski
 */

#ifndef CPIN_H_
#define CPIN_H_

#include "stdint.h"

class CPin
{
  public:
    typedef enum
    {
      PIN_IN,
      PIN_OUT
    } pin_direction_t;

    typedef enum
    {
      PIN_PORTA,
      PIN_PORTB,
      PIN_PORTC,
      PIN_PORTD,
      PIN_PORTE,
      PIN_PORTF,
    } pin_port_t;

  private:
    pin_port_t      port;
    uint8_t         pin;
    pin_direction_t direction;
    volatile uint8_t* portaddr;
    volatile uint8_t* ddr_addr;
    volatile uint8_t* pin_addr;

  public:
//    CPin(pin_port_t port, uint8_t pin_num, pin_direction_t direction);

    void Init(pin_port_t port, uint8_t pin_num, pin_direction_t direction);
    void Set();
    void Set(bool state);
    void Clr();
    void Toggle();
    void In();
    void Out();
    void Pullup();
    bool Read();
    bool State();

    virtual ~CPin() {};
};

#endif /* CPIN_H_ */
