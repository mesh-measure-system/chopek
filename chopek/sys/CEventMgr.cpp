/*
 * CEventMgr.cpp
 *
 *  Created on: 14-11-2013
 *      Author: Przemek Kieszkowski
 */

#include <stddef.h>

#include "CEventMgr.h"
#include "sysTypes.h"
#include "halRTC.h"

#include "trace.h"

CEventMgr eventMgr;


void CEventMgr::Init()
{
  CRun::Init();
  evt_list_p = NULL;
  main_fired = false;
}

void CEvent::fireEvent()
{
  if (fired < 255)
  {
    fired++;
  }
  eventMgr.setFired();
}
CEvent::~CEvent()
{
  // for eclipse happiness
}

bool CEventMgr::isIdle()
{
  return main_fired == false;
}

bool CEventMgr::registerEvent(CEvent* evt_p)
{
  // place to store new event
  CEvent** cevt_pp = &evt_list_p;
  CEvent*  cevt_p  =  evt_list_p;
  bool     duplicated = false;

//  TRACE_ERROR("Register event 0x%08lx, time 0x%08lx, ADCSRA = 0x%02x\n", (uint32_t)evt_p, (uint32_t)HAL_RTCGetTicks(), (int)ADCSRA);

  while (cevt_p != NULL)
  {
    // check for duplicated events
    if (cevt_p == evt_p)
    {
      // duplicated found - rejecting
      TRACE_ERROR("Found duplicated event 0x%08lx\n", (uint32_t)evt_p);
      duplicated = true;
      break;
    }
    cevt_pp = &cevt_p->next_item_p;
    cevt_p  =  cevt_p->next_item_p;
  }

  if (!duplicated)
  {
    // added at the end of queue
    *cevt_pp = evt_p;
    evt_p->next_item_p = NULL;
    return true;
  }
  return false;
}

void CEventMgr::unregisterEvent(CEvent* evt_p)
{
  // place to remove event
  CEvent** cevt_pp = &evt_list_p;
  CEvent*  cevt_p  =  evt_list_p;

//  TRACE_ERROR("Unregister event 0x%08lx, time 0x%08lx\n", (uint32_t)evt_p, (uint32_t)HAL_RTCGetTicks());

  while (cevt_p != NULL)
  {
    if (cevt_p == evt_p)
    {
      // found CEvent to remove
      *cevt_pp = cevt_p->next_item_p;
      cevt_p->next_item_p = NULL;
      break;
    }
    cevt_pp = &cevt_p->next_item_p;
    cevt_p  =  cevt_p->next_item_p;
  }
}

void CEventMgr::setFired()
{
  main_fired = true;
}

void CEventMgr::run()
{
  bool loc_fired;
  ATOMIC_SECTION_ENTER
  {
    loc_fired = main_fired;
    main_fired = false;
  }
  ATOMIC_SECTION_LEAVE

  if (loc_fired)
  {
//    TRACE_USER("loc_fired!\n");

    CEvent* evt_p = evt_list_p;
    while (evt_p != NULL)
    {
      if (evt_p->fired > 0)
      {
        uint8_t fired_cnt;
        ATOMIC_SECTION_ENTER
          fired_cnt = evt_p->fired;
          evt_p->fired = 0;
        ATOMIC_SECTION_LEAVE
//        TRACE_ERROR("Fire event 0x%08lx\n", (uint32_t)evt_p);

        evt_p->callback_fire(fired_cnt);
      }
      evt_p = evt_p->next_item_p;
    }
//    TRACE_USER("fired end\n");
  }
}

CEventMgr::~CEventMgr()
{
  // for eclipse happiness
}
